from Wall import Wall, rotate
from math import radians, sqrt
from WallMaterial import WallMaterial, Build
from WallConnector import WallConnector, Side, Edge
from Dimensions import D
from Position import P
from Door import Door, GarageDoor, WorkshopDoor
from Window import Window
from copy import deepcopy

class Basement:
    def __init__(self):
        Window.identify = "A"
        Door.identify = "A"
        osb16 = WallMaterial("OSB", build=Build.SOLID, dimensions=[D(height=2000,width=2800,depth=16)])
        osb16.addDimension(D(height=670,width=2050,depth=16))

        kvh8x20 = WallMaterial("KVH", build=Build.GRID, dimensions=[D(height=7000,width=80,depth=200)], distance_min=400, distance_max=900, distance_avg=500)
        kvh8x20.addDimension(D(height=4000,width=200,depth=80))

        kvh8x16 = WallMaterial("KVH", build=Build.GRID, dimensions=[D(height=7000,width=80,depth=120)], distance_min=400, distance_max=900, distance_avg=500)
        kvh8x16.addDimension(D(height=4000,width=120,depth=80))

        dwd20 = WallMaterial("DWD", build=Build.SOLID, dimensions=[D(height=2000,width=2800,depth=20)])

        wire = WallMaterial("AIR-WIRE", build=Build.EMPTY, dimensions=[D(depth=50)])

        self.w_outside = (osb16, wire, osb16, kvh8x20, dwd20)
        self.w_outsideDepth = sum(layer.dimensions[0].depth for layer in self.w_outside)
        self.w_inside = (osb16, osb16, kvh8x16, osb16, osb16)
        self.w_insideDepth = sum(layer.dimensions[0].depth for layer in self.w_inside)
        self.w_inside2 = (osb16, osb16, kvh8x20, osb16, osb16)
        self.w_insideDepth2 = sum(layer.dimensions[0].depth for layer in self.w_inside2)
        
        roof_angle = 11
        daemmung = 120
        
        suedNordlen = 4000 + 2000 + 3000 + 2000 + 1936+ 5*self.w_insideDepth + 2*self.w_outsideDepth -2*daemmung
        westOstLen = 10500+2*self.w_insideDepth -2*daemmung
        
        doorHeight = 2125
        doorWidthWide = 985
        doorWidth = 885
        
        windowHeight = 830
        windowWidth = 1230
        windowWidth2 = 1430
        
        defaultWD = D(height=windowHeight, width=windowWidth)
        defaultDD = D(height=doorHeight, width=doorWidthWide)
        
        walls = {}
        walls["west"] = Wall(self.w_outside, lenght=westOstLen, height=2560, 
            inverseWall=True)
        walls["west"].fixed = True
        walls["west"].addWindow(Window("Hobby", defaultWD, position=P(x=4000, z=1500)))
        
        walls["sued"] = Wall(self.w_outside, lenght=suedNordlen, 
            height=walls["west"].getHeight(Side.FRONT, Edge.LEFT),
            inverseWall=True)
        walls["sued"].connect(side=Side.FRONT, edge=Edge.RIGHT,
            connector=WallConnector(wall=walls["west"], side=Side.BACK, edge=Edge.LEFT))
        walls["sued"].addDoor(GarageDoor("Garagentor", dim=D(height=doorHeight, width=3500), position=P(x=550), flipy=True))
        walls["sued"].addWindow(Window("HWR", dim=D(height=windowHeight, width=windowWidth2), position=P(x=7900, z=1500)))
        walls["sued"].addWindow(Window("Brauerei", defaultWD, position=P(x=11800, z=1500)))
        
        walls["ost"] = Wall(self.w_outside, lenght=westOstLen, 
            height=walls["west"].getHeight(Side.FRONT, Edge.LEFT),
            inverseWall=True)
        walls["ost"].connect(side=Side.BACK, edge=Edge.RIGHT,
            connector=WallConnector(wall=walls["sued"], side=Side.FRONT, edge=Edge.LEFT))
        walls["ost"].addWindow(Window("Garage", defaultWD, position=P(x=8000, z=1500)))
        walls["ost"].addWindow(Window("Werkstatt", defaultWD, position=P(x=2400, z=1500)))
        
        
        walls["nord"] = Wall(self.w_outside, lenght=suedNordlen, 
            height=walls["ost"].getHeight(Side.FRONT, Edge.LEFT),
            topWall=True)
        walls["nord"].connect(side=Side.FRONT, edge=Edge.RIGHT,
            connector=WallConnector(wall=walls["ost"], side=Side.BACK, edge=Edge.LEFT))
        walls["nord"].addWindow(Window("Hobby", defaultWD, position=P(x=3000, z=1500)))
        walls["nord"].addWindow(Window("Werkstatt", defaultWD, position=P(x=5800, z=1500)))
        walls["nord"].addWindow(Window("Werkstatt", defaultWD, position=P(x=10150, z=1500)))
        
        garageLenght = 4700
        walls["garage"] = Wall(self.w_inside, lenght=5600, height=2560)
        walls["garage"].connect(side=Side.FRONT, edge=Edge.RIGHT, adaptHeight=False,
            connector=WallConnector(wall=walls["sued"], side=Side.FRONT, edge=Edge.RIGHT, offset_xy=-(walls["sued"].lenght-garageLenght-1*self.w_outsideDepth-self.w_insideDepth)))
        walls["garage"].addDoor(Door("Garage-HWR", defaultDD, position=P(x=4000), flipx=True, flipy=True))
        
        walls["garage2"] = Wall(self.w_inside, lenght=garageLenght, height=2560)
        walls["garage2"].connect(side=Side.BACK, edge=Edge.LEFT, adaptHeight=False,
            connector=WallConnector(wall=walls["garage"], side=Side.BACK, edge=Edge.LEFT))
        walls["garage2"].addDoor(WorkshopDoor("Werkstatttor", dim=D(height=doorHeight, width=2500), position=P(x=200), flipy=False))
        
        werkstattOffset = self.w_outsideDepth+4700      
        
        walls["brauGast"] = Wall(self.w_inside, lenght=werkstattOffset, height=2560)
        walls["brauGast"].connect(side=Side.FRONT, edge=Edge.RIGHT, adaptHeight=False,
            connector=WallConnector(wall=walls["sued"], side=Side.FRONT, offset_xy=-self.w_outsideDepth-4100, edge=Edge.RIGHT))
        
        walls["brauGast"].addDoor(Door("Brauerei", defaultDD, position=P(x=1700)))
        walls["brauGast"].addDoor(Door("Hobby", defaultDD, position=P(x=250), flipx=True, flipy=True))
        brauereiLen = 3400
        walls["brauGast2"] = Wall(self.w_inside, lenght=4100, height=2560)
        walls["brauGast2"].connect(side=Side.FRONT, edge=Edge.RIGHT, adaptHeight=False,
            connector=WallConnector(wall=walls["brauGast"], side=Side.FRONT, edge=Edge.LEFT, offset_xy=walls["brauGast"].lenght-brauereiLen))
        
        
        werkstattLen = walls["nord"].lenght - walls["brauGast2"].lenght - self.w_insideDepth - walls["garage2"].lenght - 2*self.w_outsideDepth - self.w_insideDepth

        walls["werkstatt"] = Wall(self.w_inside, lenght=werkstattLen, height=2560)
        walls["werkstatt"].connect(side=Side.BACK, edge=Edge.RIGHT, adaptHeight=False,
            connector=WallConnector(wall=walls["garage"], side=Side.FRONT, edge=Edge.RIGHT, offset_xy=-werkstattOffset))
        walls["werkstatt"].addDoor(Door("HWR-Werkstatt", defaultDD, position=P(x=150)))
        werkstatt2Offset = -self.w_insideDepth
        walls["werkstatt2"] = Wall(self.w_inside, lenght=walls["west"].lenght-werkstattOffset, height=2560)
        walls["werkstatt2"].connect(side=Side.BACK, edge=Edge.LEFT, adaptHeight=False,
            connector=WallConnector(wall=walls["werkstatt"], side=Side.BACK, edge=Edge.LEFT, offset_xy=werkstatt2Offset))
        
        GLOBAL_HIDING = True
        GLOBAL_HIDING_LEVEL = 5
        
        offset = walls["ost"].depth
        walls["sued"].addMeasure("Garage", "1", 0, offset, walls["garage2"].lenght)
        walls["sued"].addMeasure("Werkstatt", "1", GLOBAL_HIDING_LEVEL, offset, walls["garage2"].lenght+walls["garage"].depth, hidden=GLOBAL_HIDING)
        offset += walls["garage2"].depth + walls["garage2"].lenght
        walls["sued"].addMeasure("Treppe,HWR", "1", 0, offset, walls["werkstatt"].lenght)
        offset += walls["brauGast"].depth + walls["werkstatt"].lenght
        walls["sued"].addMeasure("Brauerei", "1", 0, offset, walls["brauGast2"].lenght)
        walls["sued"].addMeasure("Hobby", "1", GLOBAL_HIDING_LEVEL, offset, walls["brauGast2"].lenght, hidden=GLOBAL_HIDING)
        walls["sued"].addMeasure("Suedseite", "1", 1, 0, walls["sued"].lenght)
        
        walls["west"].addMeasure("Brauerei", "1", 0, 0, brauereiLen)
        offset = brauereiLen + walls["brauGast2"].depth
        lenght = walls["west"].lenght-(walls["werkstatt2"].lenght+brauereiLen+walls["brauGast2"].depth)
        walls["west"].addMeasure("Hobby", "1", 0, offset, lenght)
        offset += lenght
        walls["west"].addMeasure("Hobby", "2", 0, offset, walls["werkstatt2"].lenght)
        walls["west"].addMeasure("Werkstatt", "2", GLOBAL_HIDING_LEVEL, offset, walls["werkstatt2"].lenght, hidden=GLOBAL_HIDING)
        
        offset = walls["west"].depth
        #lenght = walls["nord"].lenght - (walls["garage2"].lenght + (walls["werkstatt"].lenght - werkstatt2Offset))
        lenght = walls["nord"].lenght - walls["ost"].depth - walls["west"].depth - (walls["werkstatt"].lenght - werkstatt2Offset) - walls["garage2"].lenght - walls["garage"].depth
        walls["nord"].addMeasure("Hobby", "2", 0, offset, lenght)
        offset += lenght + walls["werkstatt2"].depth
        lenght = walls["werkstatt"].lenght - werkstatt2Offset - walls["werkstatt2"].depth
        walls["nord"].addMeasure("Werkstatt", "2", 0, offset, lenght)
        walls["nord"].addMeasure("nordwand", "1", 1, offset, walls["nord"].lenght-offset-walls["ost"].depth)
        
        lenght = walls["ost"].lenght - walls["garage"].lenght
        walls["ost"].addMeasure("Werkstatt", "1", 1, 0, lenght)
        offset = lenght
        walls["ost"].addMeasure("Garage", "1", 1, offset+self.w_insideDepth, walls["garage"].lenght-self.w_insideDepth)
        walls["ost"].addMeasure("Treppe,HWR", "1", 0, walls["ost"].lenght-(walls["brauGast"].lenght-walls["werkstatt"].depth), walls["brauGast"].lenght-walls["werkstatt"].depth)
        walls["ost"].addMeasure("ostseite", "1", 2, -walls["nord"].depth, walls["ost"].lenght + walls["nord"].depth + walls["sued"].depth)
        
        mergedMeasures = dict()
        self.mergeMeasures(mergedMeasures, walls["sued"].measures)
        self.mergeMeasures(mergedMeasures, walls["ost"].measures)
        self.mergeMeasures(mergedMeasures, walls["nord"].measures)
        self.mergeMeasures(mergedMeasures, walls["west"].measures)
        
        (areas, totalArea) = self.calculateArea(mergedMeasures)
        offset = walls["ost"].depth
        walls["sued"].addArea("Garage", areas["Garage"],walls["garage2"].lenght/2 + offset, -walls["garage"].lenght/2)
        offset += walls["garage2"].lenght + walls["garage"].depth
        walls["sued"].addArea("Treppe,HWR", areas["Treppe,HWR"], offset + walls["werkstatt"].lenght/2, -walls["brauGast"].lenght/2)
        offset += walls["werkstatt"].lenght
        walls["sued"].addArea("Brauerei", areas["Brauerei"], offset + walls["brauGast2"].lenght/2, -3500/2)
        
        offset = walls["west"].depth
        
        walls["sued"].addArea("Hobby", areas["Hobby"], walls["sued"].lenght - (offset + 5500/2), -(4500+6000/2))
        offset += 5500 + walls["werkstatt2"].depth
        walls["sued"].addArea("Werkstatt", areas["Werkstatt"], walls["sued"].lenght - (offset + 9280/2), -(4500+7000/2))
        
        walls["sued"].addArea("Nettofläche", totalArea, walls["sued"].lenght+1000, -(walls["west"].lenght+1000))
        self.walls = walls
        
    def mergeMeasures(self, dest, src1):
        src = deepcopy(src1)
        for room in src:
            for index in src[room]:
                if room in dest:
                    if index in dest[room]:
                        dest[room][index].update(src[room][index])
                    else:
                        dest[room][index] = src[room][index]
                else:
                    dest[room] = src[room]
    
    def calculateArea(self, measures):
        totalArea = 0
        areas = dict()
        for room in measures:
            area = 0
            for index in measures[room]:
                values = list(measures[room][index].values())
                try:
                    area += values[0]["lenght"] * values[1]["lenght"]
                except:
                    pass
            areas[room] = area/1000000
            totalArea += areas[room]
        return (areas, totalArea)
    
    def scad(self):
        ret = ""
        for name in self.walls:
            ret += self.walls[name].solid("basement" + name)
        
        colors = ("yellow", "green", "red", "blue", "purple", "orange", "cyan", "grey", "orange")
        ret += "module basement(showDoors=false, showDoorMeasures=false, showWindows=false, showWindowMeasures=false, showMeasures=false, showAreas=false) {\n"
        i = 0;
        for name in self.walls:
            color = colors[i%len(colors)]
            ret += "color(\"{}\") ".format(color)
            ret += "wall_basement{}(showDoors, showDoorMeasures, showWindows, showWindowMeasures, showMeasures, showAreas);\n".format(name)
            i += 1
        ret += "}"
        
        ret += "//Bodenplatte\n"
        bodenplatteHeight = 200
        ret += "module bodenplatte_bm() {{\n translate([{tx},{ty},{tz}]) cube([{x},{y},-{tz}]);\n".format(
            y=self.walls["sued"].lenght, 
            x=self.walls["ost"].lenght+2*self.walls["sued"].depth,
            tx=-self.walls["sued"].depth,
            ty=-self.walls["sued"].lenght+self.walls["ost"].depth,
            tz=-bodenplatteHeight)
        ret += "}"
        return ret
        
    def windows(self):
        summary = dict()
        for name in self.walls:
            for window in self.walls[name].windows:
                s = "({}) {}".format(window.identify, window.name)
                if window.dim in summary:
                    summary[window.dim]["count"] += 1
                    summary[window.dim]["names"].append(s)
                else:
                    summary[window.dim] = {"count" : 1, "names" : [s], "order" : window.getOrder(False)}
        
        ret = ""
        for key in summary:
            ret += "* {}x {}\n".format(summary[key]["count"], summary[key]["order"])
            
            for name in summary[key]["names"]:
                ret += "    * {}\n".format(name)
        ret += "\n"
        return ret

    def doors(self):
        summary = dict()
        for name in self.walls:
            for door in self.walls[name].doors:
                s = "({}) {}".format(door.identify, door.name)
                if door.dim in summary:
                    summary[door.dim]["count"] += 1
                    summary[door.dim]["names"].append(s)
                else:
                    summary[door.dim] = {"count" : 1, "names" : [s], "order" : door.getOrder(False)}
        
        ret = ""
        for key in summary:
            ret += "* {}x {}\n".format(summary[key]["count"], summary[key]["order"])
            
            for name in summary[key]["names"]:
                ret += "    * {}\n".format(name)
        ret += "\n"
        return ret
