#from House import House
from House import House
from Basement import Basement
import argparse

class builder:
    def __init__(self):
        
        parser = argparse.ArgumentParser(description='Generate an OpenSCAD model for a house.')
        parser.add_argument('output', metavar='output', type=str, help='outputfile')
        parser.add_argument('-w', '--windows' ,metavar='windows', type=str, help='outputfile for window list')
        parser.add_argument('-d', '--doors', metavar='doors', type=str, help='outputfile for door list')

        args = parser.parse_args()
        
        house = House()
        basement = Basement()
        output = """use <door.scad>
use <window.scad>
use <measurement.scad>
use <area.scad>
{}
{}""".format(house.scad(), basement.scad())
    
        with open(args.output, 'w') as f:  # Use file to refer to the file object
            f.write(output)
        
        #Ausgabe der Fensterliste    
        try:
            output = "# Liste der Fenster\n\n"
            output += "## Fenster im Erdgeschoss\n"
            output += house.windows()
            output += "## Fenster im Keller\n"
            output += basement.windows()
        
            with open(args.windows, 'w') as f:  # Use file to refer to the file object
                f.write(output)
        except Exception as e:
            pass
            #print(e)
        
        #Ausgabe der Türliste 
        try:
            output = "# Liste der Türen\n\n"
            output += "## Türen im Erdgeschoss\n"
            output += house.doors()
            output += "## Türen im Keller\n"
            output += basement.doors()
            
            with open(args.doors, 'w') as f:  # Use file to refer to the file object
                f.write(output)
        except Exception as e:
            pass
            #print(e)
            
builder()
