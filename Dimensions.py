class D:
    def __init__(self, height=0, width=0, depth=0):
        self.height = height
        self.width = width
        self.depth = depth
    def __hash__(self):
        return hash((self.height, self.width, self.depth))

    def __eq__(self, other):
        return (self.height, self.width, self.depth) == (other.height, other.width, other.depth)
    def __ne__(self, other):
        # Not strictly necessary, but to avoid having both x==y and x!=y
        # True at the same time
        return not(self == other)
