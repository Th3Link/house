from Dimensions import D
from Position import P

class Door:
    identify = "A"
    
    def __init__(self, name, dim: D, position: P, flipx=False, flipy=False):
        self.dim = dim
        self.position = position
        self.flipx = 0
        self.flipy = 0
        self.name = name
        self.identify = chr(ord(Door.identify) + 1)
        Door.identify = self.identify
        if flipx:
            self.flipx = 1
        if flipy:
            self.flipy = 1
    def getHoleSCAD(self, depth):
        return """translate([{position.x}, {position.y}-1, {position.z}]) \
cube([{dim.width}, {depth}+2, {dim.height}]);""".format(position=self.position, dim=self.dim, depth=depth)
    def getSCAD(self, depth):
        return """translate([{position.x}, {position.y}-1, {position.z}]) \
door({dim.width}, {dim.height}, {depth}, {flipx}, {flipy}, visible=showDoors, id="{identify}");""".format(
            position=self.position,
            dim=self.dim,
            depth=depth,
            flipx=self.flipx,
            flipy=self.flipy,
            identify=self.identify)

    def getOrder(self, withName = True):
        name = ""
        if withName:
            name = "{name} ({identify}): ".format(
                name=self.name,
                identify=self.identify)
        return """{name}B: {lenght}, H: {height}""".format(
            name=name,
            lenght=self.dim.width,
            height=self.dim.height)

class GarageDoor(Door):
    
    def getHoleSCAD(self, depth):
        return """translate([{position.x}, {position.y}-1, {position.z}]) \
cube([{dim.width}, {depth}+2, {dim.height}]);""".format(position=self.position, dim=self.dim, depth=depth)

    def getSCAD(self, depth):

        return """translate([{position.x}, {position.y}-1, 0]) \
garageDoor({dim.width}, {dim.height}, {depth}, {flipx}, {flipy}, visible=showDoors);""".format(
            position=self.position,
            dim=self.dim,
            depth=depth,
            flipx=self.flipx,
            flipy=self.flipy)

class WorkshopDoor(Door):
            
    def getHoleSCAD(self, depth):
        return """translate([{position.x}, {position.y}-1, {position.z}]) \
cube([{dim.width}, {depth}+2, {dim.height}]);""".format(position=self.position, dim=self.dim, depth=depth)

    def getSCAD(self, depth):
        return """translate([{position.x}, {position.y}-1, 0]) \
workshopDoor({dim.width}, {dim.height}, {depth}, {flipx}, {flipy}, visible=showDoors);""".format(
            position=self.position,
            dim=self.dim,
            depth=depth,
            flipx=self.flipx,
            flipy=self.flipy)
