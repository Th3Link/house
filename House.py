from Wall import Wall, rotate
from math import radians, sqrt
from WallMaterial import WallMaterial, Build
from WallConnector import WallConnector, Side, Edge
from Dimensions import D
from Position import P
from Door import Door
from Window import Window
from copy import deepcopy

class House:
    def __init__(self):
        Window.identify = "A"
        Door.identify = "A"
        osb16 = WallMaterial("OSB", build=Build.SOLID, dimensions=[D(height=2000,width=2800,depth=16)])
        osb16.addDimension(D(height=670,width=2050,depth=16))

        kvh8x20 = WallMaterial("KVH", build=Build.GRID, dimensions=[D(height=7000,width=80,depth=200)], distance_min=400, distance_max=900, distance_avg=500)
        kvh8x20.addDimension(D(height=4000,width=200,depth=80))

        kvh8x16 = WallMaterial("KVH", build=Build.GRID, dimensions=[D(height=7000,width=80,depth=120)], distance_min=400, distance_max=900, distance_avg=500)
        kvh8x16.addDimension(D(height=4000,width=100,depth=60))

        dwd20 = WallMaterial("DWD", build=Build.SOLID, dimensions=[D(height=2000,width=2800,depth=20)])

        wire = WallMaterial("AIR-WIRE", build=Build.EMPTY, dimensions=[D(depth=50)])

        self.w_outside = (osb16, wire, osb16, kvh8x20, dwd20)
        self.w_outsideDepth = sum(layer.dimensions[0].depth for layer in self.w_outside)
        self.w_inside = (osb16, osb16, kvh8x16, osb16, osb16)
        self.w_insideDepth = sum(layer.dimensions[0].depth for layer in self.w_inside)
        self.w_inside2 = (osb16, osb16, kvh8x20, osb16, osb16)
        self.w_insideDepth2 = sum(layer.dimensions[0].depth for layer in self.w_inside2)
        
        roof_angle = 22
        
        suedNordlen = 4000 + 2000 + 3000 + 2000 + 1936+ 5*self.w_insideDepth + 2*self.w_outsideDepth
        westOstLen = 10500+2*self.w_insideDepth
        
        doorHeight = 2125
        doorWidth = 885
        doorWidthSmall = 725
        houseDoorWidth = 1130
        inwallHeight = 2500
        
        windowHeight = 1430
        windowHeight4 = 1330
        windowHeightOberlicht = 1030
        windowHeightOberlicht2 = 1230
        windowWidth4 = 1830
        windowWidth2 = 1430
        windowWidth3 = 830
        windowHeightDoor = 2125
        sideFactor = 0.53
        commonBRH = 900
        
        walls = {}
        
        walls["west"] = Wall(self.w_outside, lenght=westOstLen*sideFactor, 
            height=2500, angle=roof_angle, topWall=True)
        walls["west"].fixed = True
        walls["west2"] = Wall(self.w_outside, lenght=westOstLen*(1-sideFactor), 
            height=walls["west"].getHeight(Side.FRONT, Edge.RIGHT), angle=-roof_angle, 
            angle_edge_fix=Edge.LEFT, topWall=True)
        walls["west2"].connect(side=Side.FRONT, edge=Edge.LEFT,
            connector=WallConnector(wall=walls["west"], side=Side.FRONT, edge=Edge.RIGHT, angle=0))
            
        walls["west2"].addWindow(Window("Esszimmer",
            dim=D(height=windowHeight, width=windowWidth2), 
            position=P(x=walls["west2"].lenght-windowWidth2-700, z=commonBRH)))
        walls["west2"].addWindow(Window("Terassentür West", dim=D(height=windowHeightDoor, width=1500-840), position=P(x=walls["west2"].lenght-(1500-840)-3300)))
        walls["west2"].addDoor(Door("Terassentür West", dim=D(height=windowHeightDoor, width=840), position=P(x=walls["west2"].lenght-690-3300-600), flipy=1, flipx=0))
        #Oberlicht
        walls["west2"].addWindow(Window("Oberlicht Wohnzimmer",
            dim=D(height=windowHeightOberlicht2, width=1500), 
            position=P(x=walls["west2"].lenght-1350-3300, z=2600), angle=roof_angle, angle_edge_fix=Edge.RIGHT))
        ##Seitenfenster
        walls["west"].addWindow(Window("Wohnzimmer", dim=D(height=windowHeightDoor, width=windowWidth3), position=P(x=300)))
        walls["west"].addWindow(Window("Wohnzimmer", dim=D(height=windowHeightDoor, width=windowWidth3), position=P(x=4400)))
        #Oberlicht
        walls["west"].addWindow(Window("Oberlicht Wohnzimmer",
            dim=D(height=windowHeightOberlicht2, width=1500), 
            position=P(x=3750, z=2600), angle=roof_angle, angle_edge_fix=Edge.LEFT))
        
        walls["sued"] = Wall(self.w_outside, lenght=suedNordlen, 
            height=walls["west"].getHeight(Side.FRONT, Edge.LEFT), 
            top_angle=-roof_angle, inverseWall=True)
        walls["sued"].connect(side=Side.FRONT, edge=Edge.RIGHT,
            connector=WallConnector(wall=walls["west"], side=Side.BACK, edge=Edge.LEFT))
        walls["sued"].addWindow(Window("Wohnzimmer", dim=D(height=windowHeight4, width=windowWidth4), position=P(x=suedNordlen-3000, z=commonBRH)))
        walls["sued"].addWindow(Window("Wohnzimmer",dim=D(height=windowHeight4, width=windowWidth4), position=P(x=suedNordlen-6300, z=commonBRH)))
        walls["sued"].addWindow(Window("Schlafzimmer", dim=D(height=windowHeight4, width=windowWidth4), position=P(x=suedNordlen-9000, z=commonBRH)))
        walls["sued"].addWindow(Window("Bad", dim=D(height=windowHeight4, width=1200), position=P(x=2000, z=commonBRH)))
        
        walls["ost"] = Wall(self.w_outside, lenght=westOstLen*sideFactor, 
            height=walls["west"].getHeight(Side.FRONT, Edge.LEFT), angle=roof_angle, 
            angle_edge_fix=Edge.RIGHT, inverseWall=True)
            
        walls["ost"].connect(side=Side.BACK, edge=Edge.RIGHT,
            connector=WallConnector(wall=walls["sued"], side=Side.FRONT, edge=Edge.LEFT))
            
        walls["ost2"] = Wall(self.w_outside, lenght=westOstLen*(1-sideFactor), 
            height=walls["west"].getHeight(Side.FRONT, Edge.RIGHT), angle=-roof_angle, 
            angle_edge_fix=Edge.RIGHT, inverseWall=True)
            
        walls["ost2"].connect(side=Side.FRONT, edge=Edge.RIGHT,
            connector=WallConnector(wall=walls["ost"], side=Side.FRONT, edge=Edge.LEFT, angle=0))        
        walls["ost"].addDoor(Door("Haustür", dim=D(height=2230, width=houseDoorWidth), position=P(x=900), flipx=True, flipy=True))
        walls["ost"].addWindow(Window("Oberlicht Haustür", dim=D(height=windowHeightOberlicht2, width=houseDoorWidth), position=P(x=200, z=2600), angle=roof_angle, angle_edge_fix=Edge.RIGHT))
        #WC Fenster
        walls["ost"].addWindow(Window("WC", dim=D(height=1030, width=630), position=P(x=3200, z=1000)))
        walls["ost2"].addWindow(Window("Kinderzimmer3", dim=D(height=windowHeight, width=1430), position=P(x=walls["ost2"].lenght-1400-2650, z=commonBRH)))
        
        walls["nord"] = Wall(self.w_outside, lenght=suedNordlen, 
            height=walls["ost2"].getHeight(Side.FRONT, Edge.LEFT), 
            top_angle=roof_angle, topWall=True)
        walls["nord"].connect(side=Side.FRONT, edge=Edge.RIGHT,
            connector=WallConnector(wall=walls["ost2"], side=Side.BACK, edge=Edge.LEFT, angle=90))
        
        walls["nord"].addWindow(Window("Kinderzimmer3", dim=D(height=windowHeight, width=windowWidth4), position=P(x=suedNordlen-3000, z=commonBRH)))
        walls["nord"].addWindow(Window("Kinderzimmer2", dim=D(height=windowHeight, width=windowWidth4), position=P(x=suedNordlen-6000, z=commonBRH)))
        walls["nord"].addWindow(Window("Kinderzimmer1", dim=D(height=windowHeight, width=windowWidth4), position=P(x=suedNordlen-9000, z=commonBRH)))

        #Küche
        walls["nord"].addWindow(Window("Küche", dim=D(height=windowHeight, width=windowWidth2), position=P(x=suedNordlen-10900, z=commonBRH)))       
        walls["nord"].addWindow(Window("Terasse Nord", dim=D(height=windowHeight, width=windowWidth2), position=P(x=600, z=commonBRH)))
    
        kinder1Offset = walls["west2"].lenght-self.w_insideDepth2/2
        kinder2offset = 2900
        walls["kinder1"] = Wall(self.w_inside2, lenght=3*kinder2offset+3*self.w_insideDepth, height=inwallHeight)
        walls["kinder1"].connect(side=Side.FRONT, edge=Edge.RIGHT, adaptHeight=False,
            connector=WallConnector(wall=walls["ost2"], side=Side.FRONT, edge=Edge.LEFT, offset_xy=self.w_insideDepth2+kinder1Offset))
        walls["kinder1"].addDoor(Door("Kinderzimmer1", dim=D(height=doorHeight, width=doorWidth), position=P(x=self.w_insideDepth+200), flipy=1, flipx=0))
        walls["kinder1"].addDoor(Door("Kinderzimmer2", dim=D(height=doorHeight, width=doorWidth), position=P(x=walls["kinder1"].lenght-self.w_insideDepth-doorWidth-1700), flipy=1, flipx=0))

        walls["kinder2"] = Wall(self.w_inside, lenght=kinder1Offset, height=inwallHeight)
        walls["kinder2"].connect(side=Side.FRONT, edge=Edge.LEFT, adaptHeight=False,
            connector=WallConnector(wall=walls["kinder1"], side=Side.BACK, edge=Edge.RIGHT, offset_xy=-kinder2offset))
 
        walls["kinder3"] = Wall(self.w_inside, lenght=kinder1Offset, height=inwallHeight)
        walls["kinder3"].connect(side=Side.FRONT, edge=Edge.LEFT, adaptHeight=False,
            connector=WallConnector(wall=walls["kinder1"], side=Side.BACK, edge=Edge.RIGHT, offset_xy=-2*kinder2offset-self.w_insideDepth))
        
        walls["kinder4"] = Wall(self.w_inside, lenght=kinder1Offset, height=inwallHeight)
        walls["kinder4"].connect(side=Side.FRONT, edge=Edge.LEFT, adaptHeight=False,
            connector=WallConnector(wall=walls["kinder1"], side=Side.BACK, edge=Edge.RIGHT, offset_xy=-3*kinder2offset-2*self.w_insideDepth))
        
        bad1Offset = 3000
        walls["bad1"] = Wall(self.w_inside, lenght=3500+self.w_insideDepth, height=inwallHeight)
        walls["bad1"].connect(side=Side.FRONT, edge=Edge.RIGHT, adaptHeight=False,
            connector=WallConnector(wall=walls["ost"], side=Side.FRONT, edge=Edge.RIGHT, offset_xy=-bad1Offset))
        walls["bad1"].addDoor(Door("Bad", dim=D(height=doorHeight, width=doorWidth), position=P(x=900), flipy=1, flipx=1))
        walls["bad1"].addDoor(Door("WC", dim=D(height=doorHeight, width=doorWidthSmall), position=P(x=walls["bad1"].lenght-1550), flipy=1, flipx=0))
        
        
        walls["bad2"] = Wall(self.w_inside, lenght=bad1Offset+self.w_insideDepth, height=inwallHeight)
        walls["bad2"].connect(side=Side.FRONT, edge=Edge.RIGHT, adaptHeight=False,
            connector=WallConnector(wall=walls["bad1"], side=Side.BACK, edge=Edge.LEFT))

        walls["bad3"] = Wall(self.w_inside, lenght=1600, height=inwallHeight)
        walls["bad3"].connect(side=Side.BACK, edge=Edge.RIGHT, adaptHeight=False,
            connector=WallConnector(wall=walls["bad1"], side=Side.FRONT, edge=Edge.LEFT, offset_xy=walls["bad1"].lenght-self.w_insideDepth-1600))
            
        walls["bad4"] = Wall(self.w_inside, lenght=1600+self.w_insideDepth, height=inwallHeight)
        walls["bad4"].connect(side=Side.FRONT, edge=Edge.RIGHT, adaptHeight=False,
            connector=WallConnector(wall=walls["bad3"], side=Side.BACK, edge=Edge.LEFT))
        
        
        szWallLen = 4100
        walls["schlafen1"] = Wall(self.w_inside, lenght=szWallLen-walls["bad2"].lenght, height=inwallHeight)
        walls["schlafen1"].connect(side=Side.FRONT, edge=Edge.LEFT, adaptHeight=False,
            connector=WallConnector(wall=walls["bad1"], side=Side.BACK, edge=Edge.LEFT))
            
        walls["schlafen2"] = Wall(self.w_inside, lenght=3700, height=inwallHeight)
        walls["schlafen2"].connect(side=Side.FRONT, edge=Edge.LEFT, adaptHeight=False,
            connector=WallConnector(wall=walls["schlafen1"], side=Side.BACK, edge=Edge.RIGHT))
        walls["schlafen2"].addDoor(Door("Schlafzimmer", dim=D(height=doorHeight, width=doorWidth), position=P(x=100)))
        
        walls["schlafen3"] = Wall(self.w_inside, lenght=szWallLen, height=inwallHeight)
        walls["schlafen3"].connect(side=Side.BACK, edge=Edge.LEFT, adaptHeight=False,
            connector=WallConnector(wall=walls["schlafen2"], side=Side.FRONT, edge=Edge.RIGHT))
        
        GLOBAL_HIDING = True
        GLOBAL_HIDING_LEVEL = 5
        
        # #Südwand layer0 (details)
        offset = self.w_outsideDepth
        walls["sued"].addMeasure("Flur", "1", GLOBAL_HIDING_LEVEL, offset, walls["bad1"].lenght, hidden=GLOBAL_HIDING)
        walls["sued"].addMeasure("WC", "1", 0, offset, walls["bad3"].lenght)
        offset += self.w_insideDepth + walls["sued"].addMeasure("Bad", "1", 1, offset, walls["bad1"].lenght)
        walls["sued"].addMeasure("Flur", "2", GLOBAL_HIDING_LEVEL, offset-self.w_insideDepth, walls["schlafen2"].lenght+2*self.w_insideDepth, hidden=GLOBAL_HIDING)
        offset += self.w_insideDepth + walls["sued"].addMeasure("Schlafzimmer", "1", 0, offset, walls["schlafen2"].lenght)
        
        walls["sued"].addMeasure("Wohnzimmer", "1", 0, offset, walls["sued"].lenght-offset-self.w_outsideDepth)

        # #Südwand layer2 (ganze Seite)
        walls["sued"].addMeasure("Suedwand", "1", 2, 0, walls["sued"].lenght)
        
        #nordwand (ganze Räume)
        offset = walls["west"].depth
        lenght = walls["nord"].lenght-walls["west"].depth-walls["ost"].depth-walls["kinder1"].lenght
        offset += walls["nord"].addMeasure("Küche/Esszimmer", "1", layer=0, offset=offset, lenght=lenght) + walls["kinder4"].depth
        offset += walls["nord"].addMeasure("Kinderzimmer3", "1", layer=0, offset=offset, lenght=kinder2offset) + walls["kinder3"].depth
        offset += walls["nord"].addMeasure("Kinderzimmer2", "1", layer=0, offset=offset, lenght=kinder2offset) + walls["kinder2"].depth
        offset += walls["nord"].addMeasure("Kinderzimmer1", "1", layer=0, offset=offset, lenght=kinder2offset) + walls["kinder2"].depth
        walls["nord"].addMeasure("Nordwand", "1", layer=1, offset=0, lenght=walls["nord"].lenght)
        
        lenght = walls["bad2"].lenght-walls["bad3"].lenght - 2*self.w_insideDepth
        offset = walls["ost"].lenght-lenght
        walls["ost"].addMeasure("Bad", "1", layer=0, offset=offset, lenght=lenght)
        walls["ost"].addMeasure("Bad", "2", layer=1, offset=walls["ost"].lenght-walls["bad2"].lenght+self.w_insideDepth, lenght=walls["bad2"].lenght-self.w_insideDepth)
        walls["ost"].addMeasure("WC", "1", layer=0, offset=offset-walls["bad3"].lenght-self.w_insideDepth, lenght=walls["bad3"].lenght)
        walls["ost"].addMeasure("Flur", "1", layer=0, offset=walls["kinder1"].depth/2, lenght=walls["ost"].lenght-walls["bad2"].lenght-walls["kinder1"].depth/2)
        
        walls["ost2"].addMeasure("Ostseite", "1", layer=2, offset=0, lenght=walls["ost2"].lenght)
        walls["ost"].addMeasure("Ostseite", "1", layer=2, offset=0, lenght=walls["ost"].lenght)
        walls["ost2"].addMeasure("Ostseite", "2", layer=3, offset=-walls["nord"].depth, lenght=walls["sued"].depth + walls["ost"].lenght+walls["ost2"].lenght+walls["nord"].depth)
        
        walls["west2"].addMeasure("Kinderzimmer1", "1", layer=0, offset=walls["west2"].lenght-walls["kinder2"].lenght, lenght=walls["kinder2"].lenght)
        walls["west2"].addMeasure("Kinderzimmer2", "1", layer=GLOBAL_HIDING_LEVEL, offset=walls["west2"].lenght-walls["kinder2"].lenght, lenght=walls["kinder2"].lenght, hidden=GLOBAL_HIDING)
        walls["west2"].addMeasure("Kinderzimmer3", "1", layer=GLOBAL_HIDING_LEVEL, offset=walls["west2"].lenght-walls["kinder2"].lenght, lenght=walls["kinder2"].lenght, hidden=GLOBAL_HIDING)
        walls["west2"].addMeasure("Küche/Esszimmer", "1", layer=GLOBAL_HIDING_LEVEL, offset=walls["west2"].lenght-walls["kinder2"].lenght, lenght=walls["kinder2"].lenght, hidden=GLOBAL_HIDING)
        lenght = walls["schlafen3"].lenght -  walls["schlafen2"].depth
        walls["west"].addMeasure("Wohnzimmer", "1", layer=1, offset=0, lenght=walls["west"].lenght-walls["kinder1"].depth/2) + walls["schlafen2"].depth
        offset = walls["west"].addMeasure("Schlafzimmer", "1", layer=0, offset=0, lenght=lenght) + walls["schlafen2"].depth
        walls["west"].addMeasure("Flur", "2", layer=0, offset=offset, lenght=walls["west"].lenght-walls["schlafen2"].lenght-walls["kinder1"].depth/2)
        #walls["west"].addMeasure("Wohnzimmer", "1", layer=GLOBAL_HIDING_LEVEL, offset=0, lenght=walls["west"].lenght-walls["kinder1"].depth/2, hidden=GLOBAL_HIDING)

        mergedMeasures = dict()
        self.mergeMeasures(mergedMeasures, walls["sued"].measures)
        self.mergeMeasures(mergedMeasures, walls["ost"].measures)
        self.mergeMeasures(mergedMeasures, walls["ost2"].measures)
        self.mergeMeasures(mergedMeasures, walls["nord"].measures)
        self.mergeMeasures(mergedMeasures, walls["west"].measures)
        self.mergeMeasures(mergedMeasures, walls["west2"].measures)
        
        (areas, totalArea) = self.calculateArea(mergedMeasures)

        walls["sued"].addArea("WC", areas["WC"],1700, -3500)
        walls["sued"].addArea("Bad", areas["Bad"],3000, -1800)
        walls["sued"].addArea("Schlafzimmer", areas["Schlafzimmer"],7000, -1800)
        walls["sued"].addArea("Wohnzimmer", areas["Wohnzimmer"],12500, -2500)
        walls["sued"].addArea("Küche/Esszimmer", areas["Küche/Esszimmer"],12500, -8500)
        walls["sued"].addArea("Kinderzimmer1", areas["Kinderzimmer1"],8300, -8500)
        walls["sued"].addArea("Kinderzimmer2", areas["Kinderzimmer2"],8300-2800-300, -8500)
        walls["sued"].addArea("Kinderzimmer3", areas["Kinderzimmer3"],8300-2*2800-2*300, -8500)
        walls["sued"].addArea("Flur", areas["Flur"],8300-2*2800-2*300, -5500)
        walls["sued"].addArea("Nettofläche", totalArea, walls["sued"].lenght+1000, -(walls["west"].lenght+walls["west2"].lenght+1000))
        
        #for wall in walls:
        #    walls[wall].setHeight(6000)
        #
        self.walls = walls

    def mergeMeasures(self, dest, src1):
        src = deepcopy(src1)
        for room in src:
            for index in src[room]:
                if room in dest:
                    if index in dest[room]:
                        dest[room][index].update(src[room][index])
                    else:
                        dest[room][index] = src[room][index]
                else:
                    dest[room] = src[room]

    def calculateArea(self, measures):
        totalArea = 0
        areas = dict()
        for room in measures:
            area = 0
            for index in measures[room]:
                values = list(measures[room][index].values())
                try:
                    area += values[0]["lenght"] * values[1]["lenght"]
                except:
                    pass
            areas[room] = area/1000000
            totalArea += areas[room]
        return (areas, totalArea)

    def scad(self):
        ret = ""
        for name in self.walls:
            ret += self.walls[name].solid("house" + name)
        
        colors = ("yellow", "green", "black", "red", "blue", "green", "purple", "orange", "cyan", "grey", "orange")
        ret += "\nmodule house(showDoors=false, showDoorMeasures=false, showWindows=false, showWindowMeasures=false, showMeasures=false, showAreas=false) {\n"
        i = 0;
        for name in self.walls:
            #ret += "color(\"{}\")".format(colors[i%len(colors)])
            ret += " wall_house{}(showDoors, showDoorMeasures, showWindows, showWindowMeasures, showMeasures, showAreas);\n".format(name)
            i += 1
        ret += "}"

        ret += "//Bodenplatte\n"
        bodenplatteHeight = 200
        ret += "module bodenplatte() {{\n translate([{tx},{ty},{tz}]) difference() {{ cube([{x},{y},-{tz}]);\n".format(
            y=self.walls["sued"].lenght, 
            x=self.walls["ost"].lenght+self.walls["ost2"].lenght+2*self.walls["sued"].depth,
            tx=-self.walls["sued"].depth,
            ty=-self.walls["sued"].lenght+self.walls["ost"].depth,
            tz=-bodenplatteHeight)
        ret += "translate([{tx},{ty},-1]) cube([{x},{y},-{z}+2]);".format(
            x=854,
            y=3000,
            tx=self.walls["schlafen2"].lenght+self.walls["sued"].depth,
            ty=self.walls["bad1"].lenght+self.walls["ost"].depth+self.walls["bad2"].depth+self.walls["schlafen1"].lenght-3000,
            z=-bodenplatteHeight)
        ret += "translate([{tx},{ty},-1]) cube([{x},{y},-{z}+2]);".format(
            x=self.walls["bad1"].depth,
            y=self.walls["bad1"].depth,
            tx=self.walls["sued"].depth+1200,
            ty=self.walls["ost"].depth,
            z=-bodenplatteHeight)
        ret += "translate([{tx},{ty},-1]) cube([{x},{y},-{z}+2]);".format(
            x=self.walls["bad1"].depth,
            y=self.walls["bad1"].depth,
            tx=self.walls["ost"].lenght+self.walls["ost2"].lenght+self.walls["sued"].depth-self.walls["bad1"].depth,
            ty=self.walls["ost"].depth+self.walls["kinder1"].lenght-120,
            z=-bodenplatteHeight)
        ret += "translate([{tx},{ty},-1]) cube([{x},{y},-{z}+2]);".format(
            x=self.walls["bad1"].depth,
            y=600,
            tx=self.walls["sued"].depth,
            ty=self.walls["ost"].depth+self.walls["bad1"].lenght+self.walls["schlafen1"].lenght+2*self.walls["bad2"].depth,
            z=-bodenplatteHeight)
        ret += "}}"
        ret += "//Veranda\n"
        veradaDepth = 2500
        ret += "module veranda() {{\ntranslate([{tx},{ty},{tz}]) cube([{x},{y},-{tz}]);\n".format(
            y=veradaDepth, 
            x=self.walls["ost"].lenght+self.walls["ost2"].lenght+2*self.walls["sued"].depth,
            tx=-self.walls["sued"].depth,
            ty=self.walls["ost"].depth,
            tz=-bodenplatteHeight)
        sideLenght = 6500
        sideDepth = 4500
        ret += "translate([{tx},{ty},{tz}]) cube([{x},{y},-{tz}]);\n}}\n".format(
            y=sideLenght, 
            x=sideDepth,
            tx=self.walls["sued"].depth+self.walls["ost"].lenght+self.walls["ost2"].lenght,
            ty=self.walls["ost"].depth-(sideLenght-veradaDepth),
            tz=-bodenplatteHeight)
        
        return ret
    
        
    def windows(self):
        summary = dict()
        for name in self.walls:
            for window in self.walls[name].windows:
                s = "({}) {}".format(window.identify, window.name)
                if window.dim in summary:
                    summary[window.dim]["count"] += 1
                    summary[window.dim]["names"].append(s)
                else:
                    summary[window.dim] = {"count" : 1, "names" : [s], "order" : window.getOrder(False)}
        
        ret = ""
        for key in summary:
            ret += "* {}x {}\n".format(summary[key]["count"], summary[key]["order"])
            
            for name in summary[key]["names"]:
                ret += "    * {}\n".format(name)
        ret += "\n"
        return ret

    def doors(self):
        summary = dict()
        for name in self.walls:
            for door in self.walls[name].doors:
                s = "({}) {}".format(door.identify, door.name)
                if door.dim in summary:
                    summary[door.dim]["count"] += 1
                    summary[door.dim]["names"].append(s)
                else:
                    summary[door.dim] = {"count" : 1, "names" : [s], "order" : door.getOrder(False)}
        
        ret = ""
        for key in summary:
            ret += "* {}x {}\n".format(summary[key]["count"], summary[key]["order"])
            
            for name in summary[key]["names"]:
                ret += "    * {}\n".format(name)
        ret += "\n"
        return ret
