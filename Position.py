class P:
    def __init__(self, x=0, y=0, z=0):
        self.x = x
        self.y = y
        self.z = z
        
    def __add__(self, b):
        return P(x=self.x + b.x, y=self.y + b.y, z=self.z + b.z)
        
    def __sub__(self, b):
        return P(x=self.x - b.x, y=self.y - b.y, z=self.z - b.z)
        
    def __repr__(self):
        return """<x={a.x}, y={a.y}, z={a.z}>""".format(a=self)

    def __str__(self):
        return """[{a.x},{a.y},{a.z}]""".format(a=self)
