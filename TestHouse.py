from Wall import Wall
from WallMaterial import WallMaterial, Build
from WallConnector import WallConnector, Side, Edge
from Dimensions import D
from Position import P
from Door import Door
from Window import Window

class House:
    def __init__(self):

        osb16 = WallMaterial("OSB", build=Build.SOLID, dimensions=[D(height=2000,width=2800,depth=16)])
        osb16.addDimension(D(height=670,width=2050,depth=16))

        kvh8x20 = WallMaterial("KVH", build=Build.GRID, dimensions=[D(height=7000,width=80,depth=200)], distance_min=400, distance_max=900, distance_avg=500)
        kvh8x20.addDimension(D(height=4000,width=200,depth=80))

        dwd20 = WallMaterial("DWD", build=Build.SOLID, dimensions=[D(height=2000,width=2800,depth=20)])

        wire = WallMaterial("AIR-WIRE", build=Build.EMPTY, dimensions=[D(depth=40)])

        self.w_outside = (osb16, wire, osb16, kvh8x20, dwd20)
        self.w_inside = (osb16, wire, osb16, kvh8x20, osb16, wire, osb16)
        
        self.w1 = Wall("1", self.w_outside, lenght=10000, height=3000)
        self.w1.fixed = True
        self.w1.addDoor(Door(dim=D(height=2100, width=900), position=P(x=2000)))
        self.w1.addWindow(Window(dim=D(height=1000, width=1200), position=P(x=3000,z=800)))
        
        self.w2 = Wall("2", self.w_outside, lenght=10000, height=2500)
        self.w2.connect(side=Side.FRONT, edge=Edge.RIGHT,
            connector=WallConnector(wall=self.w1, side=Side.BACK, edge=Edge.LEFT), adapt=True)
        
        self.w3 = Wall("3", self.w_outside, lenght=10000, height=2500)
        self.w3.connect(side=Side.FRONT, edge=Edge.LEFT,
            connector=WallConnector(wall=self.w1, side=Side.FRONT, edge=Edge.RIGHT), adapt=True)
        
        self.w4 = Wall("4", self.w_outside, lenght=10000, height=2500)
        self.w4.connect(side=Side.BACK, edge=Edge.LEFT,
            connector=WallConnector(wall=self.w1, side=Side.BACK, edge=Edge.RIGHT))
        
        print(self.w1.solid)
        print(self.w2.solid)
        print(self.w3.solid)
        print(self.w4.solid)
        
