import re
from math import tan, radians, degrees, sin, cos
from typing import Tuple

from Door import Door
from Window import Window
from WallMaterial import WallMaterial
from WallConnector import WallConnector, Side, Edge
from Position import P

"""
Maße einer Wand
8 x (x,y,z) oder
lenght, height1, height2, offset oder
lenght, height, offset, top_angle
"""
class Wall:
    def __init__(self,
                wall_material: Tuple[WallMaterial],
                lenght : int,
                height=0,
                offset_z=0,
                angle=0,
                angle_edge_fix=Edge.LEFT,
                top_angle=0,
                top_angle_side_fix=Side.FRONT,
                topWall=False,
                inverseWall=False
                ):
        
        self.wall_material = wall_material
        self.__position = P()
        self.__rotation = 0
        self.__originString = ""
        self.lenght = lenght
        self.fixed = False
        self.originString = ""
        self.height = [[height for x in range(2)] for y in range(2)] 
        self.setHeight(height)
        self.angle_edge_fix = angle_edge_fix
        self.top_angle_side_fix = top_angle_side_fix
        self.angle = angle
        self.top_angle = top_angle
        self.adaptAngle()
        self.topWall = topWall
        self.inverseWall = inverseWall
        
        self.offset_z = offset_z
        self.doors = list()
        self.windows = list()
        self.measures = dict()
        self.areas = list()
        #self.addMeasure("u","b",0,1000)
        
    def addArea(self, room, area, offsetx, offsety):
        self.areas.append({"room" : room, "area" : area, "x" : offsetx, "y" : offsety})
    
    def addMeasure(self, room, i, layer, offset, lenght, hidden=False):
        measure = { str(self.rotation) : {
            "offset": offset, 
            "lenght" : lenght, 
            "layer" : layer,
            "hidden" : hidden}}
            
        if room in self.measures:
            if i in self.measures[room]:
                self.measures[room][i].update(measure)
            else:
                self.measures[room][i] = measure
        else:
            self.measures[room] = { i : measure }
        return lenght
          
        
    def setHeight(self, height):
        self.height[Side.FRONT][Edge.LEFT] = height
        self.height[Side.FRONT][Edge.RIGHT] = height
        self.height[Side.BACK][Edge.LEFT] = height
        self.height[Side.BACK][Edge.RIGHT] = height
    
    def adaptAngle(self):

        if self.angle_edge_fix == Edge.LEFT:
            self.height[Side.FRONT][Edge.RIGHT] += tan(radians(self.angle)) * self.lenght
            self.height[Side.BACK][Edge.RIGHT] += tan(radians(self.angle)) * self.lenght 
        else:
            self.height[Side.FRONT][Edge.LEFT] += tan(radians(self.angle)) * self.lenght
            self.height[Side.BACK][Edge.LEFT] += tan(radians(self.angle)) * self.lenght

        
    def getHeight(self, side : Side, edge : Edge, offset=0):
        if side == side.BACK:
            offset += self.depth
        """
        Strahlensatz: ZA' / ZA = A'B' / AB
           
                         B'
                    B    .
                    .    |
               .    |    |
          .         |    |
        Z ----------|----|
                    A    A'
        """
        return self.height[side][edge] * (self.lenght - offset) / self.lenght;
        
    def connect(self, side, edge, connector, adaptHeight=False):
        c = WallConnector(wall=self, side=side, edge=edge)
        connector.relation = c
        self.position = connector.wall.position
        self.rotation = connector.wall.rotation
        
        origin_position, origin_rotation = self.origin(side, edge)

        target_position, target_rotation = self.target(connector)
        target_position += self.position
        self.rotation += target_rotation
        self.position = target_position + rotate(origin_position,radians(self.rotation))
        
        if adaptHeight:
            self.adaptHeight(side, edge, connector)
        
        self.__originString = """/*pos: {position}*/
/* target */ translate({t_position}) \
rotate([0,0,{rotation}])
/* origin */ translate({o_position})""".format(
            position=self.position,
            t_position=target_position,
            o_position=origin_position,
            rotation=self.rotation)
    
    def adaptHeight(self, side, edge, connector):
        self.setHeight(connector.wall.getHeight(connector.side, connector.edge, connector.offset_xy))
        self.angle_edge_fix = edge
        self.top_angle_side_fix = side
        self.angle = connector.wall.top_angle
        self.top_angle = connector.wall.angle
        self.adaptAngle()
                
    def addWindow(self, window: Window):
        self.windows.append(window)
        
    def addDoor(self, door: Door):
        self.doors.append(door)
    
    def target(self, connector):
        rotation = connector.angle
        p = P(x=connector.offset_xy, z=connector.offset_z)
        if connector.side == Side.BACK:
            p += P(y=connector.wall.depth)
        if connector.edge == Edge.RIGHT:
            p += P(x=connector.wall.lenght)
        return rotate(p, radians(connector.wall.rotation)), rotation
    
    def origin(self, side: Side, edge: Edge):
        p = P()
        rotation = 0;
        if side == Side.BACK:
            p.y = -self.depth
            rotation=180
        if edge == Edge.RIGHT: 
            p.x = -self.lenght
        return p, rotation
        
    @property
    def depth(self):
        return sum(layer.dimensions[0].depth for layer in self.wall_material)
    
    @property
    def position(self):
        return self.__position
    
    @position.setter
    def position(self, position: P):
        self.__position = position
    
    @property
    def rotation(self):
        return self.__rotation
    
    @rotation.setter
    def rotation(self, rotation: int):
        self.__rotation = rotation

    def solid(self, name):
        tw = "false"
        iw = "false"
        if self.topWall or self.inverseWall:
            tw = "true"
        if self.inverseWall:
            iw = "true"
        
        measures = ""
        for k in self.measures:
            for l in self.measures[k]:
                for m in self.measures[k][l]:
                    if not self.measures[k][l][m]["hidden"]:
                        measures += """translate([{offset},0,0]) linear_extrude({height}) \
            measurement({lenght}, 1600+400*{layer}, {topWall}, {inverseWall}, visible=showMeasures);""".format(
                        offset=self.measures[k][l][m]["offset"],
                        topWall=tw,
                        inverseWall=iw,
                        lenght=self.measures[k][l][m]["lenght"],
                        layer=self.measures[k][l][m]["layer"],
                        height=self.height[0][0])
        
        areas = ""
        for k in self.areas:
            areas += """translate([{x},{y},0]) linear_extrude({height}) roomArea("{room}", {area}, {topWall}, visible=showAreas);""".format(
                room=k["room"],
                area=k["area"],
                x=k["x"],
                y=k["y"],
                topWall=tw,
                inverseWall=iw,
                height=self.height[0][0])
        
        regex = re.compile('[^a-zA-Z0-9]')
        return """module {module_name}(showDoors, showDoorMeasures, showWindows, showWindowMeasures, showMeasures, showAreas) \
{origin}
    union() {{
    difference() {{
    {base}
    {holes}
    }}
    {windows}
    {doors}
    {measures}
    {areas}
    }}
    """.format(
            module_name="wall_"+regex.sub('',name),
            origin=self.__originString,
            base=self.base,
            holes=self.holes,
            windows=self.windowsSCAD,
            doors=self.doorsSCAD,
            measures=measures,
            areas=areas
            )

    @property
    def base(self):
        body = self.body_cube
        if not (self.height[0][0] == self.height[0][1] and 
            self.height[0][0] == self.height[1][0] and 
            self.height[0][0] == self.height[1][1]):
            body = self.body_polyhedron
        
        return """union() {{
    {body}
    
    }}""".format(
            body=body)

    @property
    def body_cube(self):
        return """translate([0,0,{offsetz}]) cube([{lenght}, {depth}, {height}]);""".format(
            lenght=self.lenght,
            height=self.height[0][0],
            depth=self.depth,
            offsetz=self.offset_z)
            
    @property
    def body_polyhedron(self):

        return """polyhedron([
    [0,0,{offset}],                     //0: U V L
    [0,{depth},{offset}],               //1: U H L
    [{lenght},{depth},{offset}],        //2: U H R
    [{lenght},0,{offset}],              //3: U V R
    [0,0,{height[0][1]}],               //4: O V L
    [0,{depth},{height[1][1]}],         //5: O H L
    [{lenght},{depth},{height[1][0]}],  //6: O H R
    [{lenght},0,{height[0][0]}]         //7: O V R
    ],[
    [0,4,7,3], // V
    [2,6,5,1], // H
    [0,3,2,1], // U
    [4,5,6,7], // O
    [0,1,5,4], // L
    [3,7,6,2]  // R
    ]);""".format(
            lenght=self.lenght,
            height=self.height,
            depth=self.depth,
            offset=self.offset_z)
            
    @property
    def holes(self):
        res = ""
        for window in self.windows:
           res += window.getHoleSCAD(self.depth+2)
        res += "\n"
        for door in self.doors:
            res += door.getHoleSCAD(self.depth+2)
        return res
    @property
    def windowsSCAD(self):
        res = ""
        for window in self.windows:
           res += window.getSCAD(self.depth+2, self.topWall, self.inverseWall)
        res += "\n"
        return res
    @property
    def doorsSCAD(self):
        res = ""
        for door in self.doors:
           res += door.getSCAD(self.depth+2)
        res += "\n"
        return res
        
def rotate(pos: P, angle, origin: P = P()):
    """
    Rotate a point counterclockwise by a given angle around a given origin.

    The angle should be given in radians.
    angle = 90
    pos.x = round(origin.x + 0 * (pos.x - origin.x) - 1 * (pos.y - origin.y),3)
    pos.y = round(origin.y + 1 * (pos.x - origin.x) + 0 * (pos.y - origin.y),3)
    
    """
    return P(x=round(origin.x + cos(angle) * (pos.x - origin.x) - sin(angle) * (pos.y - origin.y),3),
    y=round(origin.y + sin(angle) * (pos.x - origin.x) + cos(angle) * (pos.y - origin.y),3))
