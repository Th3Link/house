from enum import IntEnum

class Side(IntEnum):
    FRONT = 0,
    BACK = 1

class Edge(IntEnum):
    RIGHT = 0,
    LEFT = 1

class WallConnector:
    
    def __init__(self, wall, side: Side, edge: Edge, offset_xy=0, offset_z=0, angle=90):
        self.wall = wall
        self.side = side
        self.edge = edge
        self.offset_xy = offset_xy
        self.offset_z = offset_z
        self.angle = angle
        self.__relation = None
    
    @property
    def _relation(self):
        return self.__relation
    
    @_relation.setter
    def _relation(self, relation):
        self.__relation = relation

