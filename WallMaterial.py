from enum import Enum
from typing import List

from Dimensions import D

class Build(Enum):
    SOLID = 0,
    GRID = 1,
    EMPTY = 2

class WallMaterial:
    def __init__(self, name: str, build: Build, dimensions: List[D], distance_min=0, distance_max=0, distance_avg=0):
        self.name = name
        self.build = build
        self.dimensions = dimensions
        self.distance_min = distance_min
        self.distance_max = distance_max
        self.distance_avg = distance_avg
        
    def addDimension(self, dimension: D):
        self.dimensions.append(dimension)
