from Dimensions import D
from Position import P
from WallConnector import Side, Edge
from math import tan, radians

class Window:
    identify = "A"
    
    def __init__(self, name, dim: D, position: P, angle=0, angle_edge_fix=Edge.LEFT):
        self.dim = dim
        self.position = position
        self.angle = angle
        self.angle_edge_fix = angle_edge_fix
        self.height = [[self.dim.height for x in range(2)] for y in range(2)] 
        self.name = name
        self.identify = chr(ord(Window.identify) + 1)
        Window.identify = self.identify
        if self.angle_edge_fix == Edge.RIGHT:
            self.height[Side.FRONT][Edge.RIGHT] -= tan(radians(self.angle)) * self.dim.width
            self.height[Side.BACK][Edge.RIGHT] -= tan(radians(self.angle)) * self.dim.width 
        else:
            self.height[Side.FRONT][Edge.LEFT] -= tan(radians(self.angle)) * self.dim.width
            self.height[Side.BACK][Edge.LEFT] -= tan(radians(self.angle)) * self.dim.width
    
    def setName(name):
        self.name = name
    
    def getHoleSCAD(self, depth):

        return """translate([{position.x}, {position.y}-1, {position.z}]) polyhedron([
    [0,0,{offset}],                     //0: U V L
    [0,{depth},{offset}],               //1: U H L
    [{lenght},{depth},{offset}],        //2: U H R
    [{lenght},0,{offset}],              //3: U V R
    [0,0,{height[0][1]}],               //4: O V L
    [0,{depth},{height[1][1]}],         //5: O H L
    [{lenght},{depth},{height[1][0]}],  //6: O H R
    [{lenght},0,{height[0][0]}]         //7: O V R
    ],[
    [0,4,7,3], // V
    [2,6,5,1], // H
    [0,3,2,1], // U
    [4,5,6,7], // O
    [0,1,5,4], // L
    [3,7,6,2]  // R
    ]);""".format(
            position=self.position,
            lenght=self.dim.width,
            height=self.height,
            depth=depth,
            offset=0)

    def getSCAD(self, depth, topWall, inverseWall):
        tw = "false"
        iw = "false"
        if topWall:
            tw = "true"
        if inverseWall:
            iw = "true"
        return """translate([{position.x}, {position.y}-1, 0]) \
window({position.z}, [{dim.width}], {dim.height}, {depth}, flipy={inverseWall}, flipText={topWall}, withText=showWindowMeasures, visible=showWindows, id="{identify}");""".format(
            position=self.position,
            dim=self.dim,
            depth=depth,
            inverseWall=iw,
            topWall=tw,
            identify=self.identify)
            
    def getOrder(self, withName = True):
        name = ""
        if withName:
            name = "{name} ({identify}): ".format(
                name=self.name,
                identify=self.identify)
        if self.height[1][0] == self.height[1][1]:
            return """{name}B: {lenght}, H: {height[1][0]}""".format(
                name=name,
                lenght=self.dim.width,
                height=self.height)
        else:
            return """{name}B: {lenght}, H1: {height[1][0]} H2: {height[1][1]}""".format(
                name=name,
                lenght=self.dim.width,
                height=self.height)
