use <isocpeur.ttf>

textSize = 300;
spacing = 20;
module roomArea(roomName, area, flipy=false, visible=true) {
if (visible) {
mirror([(flipy ? 1 : 0),0, 0])  mirror([0,(flipy ? 1 : 0), 0])  union() {
translate([0,spacing,0]) offset(delta=-textSize/30) text(roomName, size=textSize, valign="bottom", halign="center", font="isocpeur");
translate([0,-spacing,0]) offset(delta=-textSize/30) text(str(round(area*100)/100, " m²"), size=textSize, valign="top", halign="center", font="isocpeur");
}
}
}
roomArea("Wohnzimmer", 20.55666, false);