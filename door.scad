use <isocpeur.ttf>

epsilon = 0.01;
dd = 60; // drawing depth
rahmen = 80;
bsh = 900;
free  = 15;
b = [1400];
h = 1400;
d = 200;
textSize = 200;
lineWidth = 600;
module door(width, height, depth, flipx=0, flipy=0, visible=true, id="", withText=true, flipText=false) {
if (visible) {
translate([(width+dd)*flipx,depth*(1-flipy),0]) mirror([0,flipy,0]) mirror([flipx,0,0]) doorHelper(width, height, depth);
if (withText) {
f2 = (flipText ? -1 : 1);
upperText = str(id, " ", str(width/10));
translate([(width-f2*lineWidth)/2,(flipy ? -textSize-50 : depth+textSize+50),0]) rotate([0,0,(flipText ? 180 : 0)]) doorText(upperText,str(height/10), height);
}
}
}

module doorHelper(width, height, depth) {
cube([dd, width, height]);
intersection() {
difference() {
translate([dd/2,0,0]) cylinder(r=width, h=height);
translate([dd/2,0,-1]) cylinder(r=width-epsilon, h=height+1);
}
cube([width+dd/2,width,height]);
}
}
module garageDoor(width, height, depth, flipx=0, flipy=0, visible=true, withText=true, flipText=false) {
if (visible) {
translate([0,depth*flipy,0]) mirror([0,flipy,0]) translate([free,0,0]) union() {
cube([width-2*free-epsilon,depth-epsilon,height-free]);
translate([0,depth,0]) cube([width-2*free-epsilon,dd-epsilon,height-free]);
}
if (withText) {
f2 = (flipy ? -1 : 1);    
translate([(width-f2*lineWidth)/2,(flipy ? -textSize-140 : depth+textSize+140),0]) rotate([0,0,(flipy ? 180 : 0)]) doorText(str(width/10),str(height/10), height);
}
}
}
module workshopDoor(width, height, depth, flipx=0, flipy=0, visible=true, withText=true, flipText=false) {
if (visible) {
translate([0,depth*flipy,0]) mirror([0,flipy,0]) translate([free,0,0]) union() {
cube([width-2*free-epsilon,depth-epsilon,height-free]);
translate([0,depth,0]) cube([width-2*free-epsilon,dd-epsilon,height-free]);
}
if (withText) {
f2 = (flipText ? -1 : 1);    
translate([(width-f2*lineWidth)/2,(flipy ? -textSize-140 : depth+textSize+140),0]) rotate([0,0,(flipText ? 180 : 0)]) doorText(str(width/10),str(height/10), height);
}
}
}

module doorText(topText, bottomText, height, flip=false, lineWidth=lineWidth) {
    textSize = 230;
    translate([(flip ? -lineWidth : 0),0,0]) linear_extrude(height) {
         
    translate([lineWidth/2,textSize/5,0]) offset(delta=-textSize/30) text(topText, size=textSize, halign="center", valign="bottom", font="isocpeur");
    offset(delta=-textSize/30) projection(true) rotate([0,90,0]) cylinder(r=textSize/20,h=lineWidth);
        
    translate([lineWidth/2,textSize/-5,0]) offset(delta=-textSize/30) text(bottomText, size=textSize, halign="center", valign="top", font="isocpeur");
    }
}

door(900, 2100, 200, flipx=1, flipy=0);
*translate([1200,0,0]) door(900, 2100, 200, flipx=1, flipy=0, flipText=true);
*projection(true) door(900, 2100, 200, 1);
*projection(true) door(900, 2100, 200, 0, 1);
*projection(true) door(900, 2100, 200, 1, 1);

 *garageDoor(900, 2100, 200, 1, 0, flipText=false);