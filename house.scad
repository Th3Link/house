use <door.scad>
use <window.scad>
use <measurement.scad>
use <area.scad>
module wall_housewest(showDoors, showDoorMeasures, showWindows, showWindowMeasures, showMeasures, showAreas) 
    union() {
    difference() {
    union() {
    polyhedron([
    [0,0,0],                     //0: U V L
    [0,302,0],               //1: U H L
    [5760.04,302,0],        //2: U H R
    [5760.04,0,0],              //3: U V R
    [0,0,2500],               //4: O V L
    [0,302,2500],         //5: O H L
    [5760.04,302,4827.207221859537],  //6: O H R
    [5760.04,0,4827.207221859537]         //7: O V R
    ],[
    [0,4,7,3], // V
    [2,6,5,1], // H
    [0,3,2,1], // U
    [4,5,6,7], // O
    [0,1,5,4], // L
    [3,7,6,2]  // R
    ]);
    
    }
    translate([300, 0-1, 0]) polyhedron([
    [0,0,0],                     //0: U V L
    [0,304,0],               //1: U H L
    [830,304,0],        //2: U H R
    [830,0,0],              //3: U V R
    [0,0,2125.0],               //4: O V L
    [0,304,2125.0],         //5: O H L
    [830,304,2125],  //6: O H R
    [830,0,2125]         //7: O V R
    ],[
    [0,4,7,3], // V
    [2,6,5,1], // H
    [0,3,2,1], // U
    [4,5,6,7], // O
    [0,1,5,4], // L
    [3,7,6,2]  // R
    ]);translate([4400, 0-1, 0]) polyhedron([
    [0,0,0],                     //0: U V L
    [0,304,0],               //1: U H L
    [830,304,0],        //2: U H R
    [830,0,0],              //3: U V R
    [0,0,2125.0],               //4: O V L
    [0,304,2125.0],         //5: O H L
    [830,304,2125],  //6: O H R
    [830,0,2125]         //7: O V R
    ],[
    [0,4,7,3], // V
    [2,6,5,1], // H
    [0,3,2,1], // U
    [4,5,6,7], // O
    [0,1,5,4], // L
    [3,7,6,2]  // R
    ]);translate([3750, 0-1, 2600]) polyhedron([
    [0,0,0],                     //0: U V L
    [0,304,0],               //1: U H L
    [1500,304,0],        //2: U H R
    [1500,0,0],              //3: U V R
    [0,0,623.9606612472649],               //4: O V L
    [0,304,623.9606612472649],         //5: O H L
    [1500,304,1230],  //6: O H R
    [1500,0,1230]         //7: O V R
    ],[
    [0,4,7,3], // V
    [2,6,5,1], // H
    [0,3,2,1], // U
    [4,5,6,7], // O
    [0,1,5,4], // L
    [3,7,6,2]  // R
    ]);

    }
    translate([300, 0-1, 0]) window(0, [830], 2125, 304, flipy=false, flipText=true, withText=showWindowMeasures, visible=showWindows, id="E");translate([4400, 0-1, 0]) window(0, [830], 2125, 304, flipy=false, flipText=true, withText=showWindowMeasures, visible=showWindows, id="F");translate([3750, 0-1, 0]) window(2600, [1500], 1230, 304, flipy=false, flipText=true, withText=showWindowMeasures, visible=showWindows, id="G");

    

    translate([0,0,0]) linear_extrude(4827.207221859537)             measurement(5628.04, 1600+400*1, true, false, visible=showMeasures);translate([0,0,0]) linear_extrude(4827.207221859537)             measurement(3916, 1600+400*0, true, false, visible=showMeasures);translate([4100,0,0]) linear_extrude(4827.207221859537)             measurement(1928.04, 1600+400*0, true, false, visible=showMeasures);
    
    }
    module wall_housewest2(showDoors, showDoorMeasures, showWindows, showWindowMeasures, showMeasures, showAreas) /*pos: [5760.04,0.0,0]*/
/* target */ translate([5760.04,0.0,0]) rotate([0,0,0])
/* origin */ translate([0,0,0])
    union() {
    difference() {
    union() {
    polyhedron([
    [0,0,0],                     //0: U V L
    [0,302,0],               //1: U H L
    [5107.96,302,0],        //2: U H R
    [5107.96,0,0],              //3: U V R
    [0,0,4827.207221859537],               //4: O V L
    [0,302,4827.207221859537],         //5: O H L
    [5107.96,302,2763.4574213425894],  //6: O H R
    [5107.96,0,2763.4574213425894]         //7: O V R
    ],[
    [0,4,7,3], // V
    [2,6,5,1], // H
    [0,3,2,1], // U
    [4,5,6,7], // O
    [0,1,5,4], // L
    [3,7,6,2]  // R
    ]);
    
    }
    translate([2977.96, 0-1, 900]) polyhedron([
    [0,0,0],                     //0: U V L
    [0,304,0],               //1: U H L
    [1430,304,0],        //2: U H R
    [1430,0,0],              //3: U V R
    [0,0,1430.0],               //4: O V L
    [0,304,1430.0],         //5: O H L
    [1430,304,1430],  //6: O H R
    [1430,0,1430]         //7: O V R
    ],[
    [0,4,7,3], // V
    [2,6,5,1], // H
    [0,3,2,1], // U
    [4,5,6,7], // O
    [0,1,5,4], // L
    [3,7,6,2]  // R
    ]);translate([1147.96, 0-1, 0]) polyhedron([
    [0,0,0],                     //0: U V L
    [0,304,0],               //1: U H L
    [660,304,0],        //2: U H R
    [660,0,0],              //3: U V R
    [0,0,2125.0],               //4: O V L
    [0,304,2125.0],         //5: O H L
    [660,304,2125],  //6: O H R
    [660,0,2125]         //7: O V R
    ],[
    [0,4,7,3], // V
    [2,6,5,1], // H
    [0,3,2,1], // U
    [4,5,6,7], // O
    [0,1,5,4], // L
    [3,7,6,2]  // R
    ]);translate([457.96000000000004, 0-1, 2600]) polyhedron([
    [0,0,0],                     //0: U V L
    [0,304,0],               //1: U H L
    [1500,304,0],        //2: U H R
    [1500,0,0],              //3: U V R
    [0,0,1230],               //4: O V L
    [0,304,1230],         //5: O H L
    [1500,304,623.9606612472649],  //6: O H R
    [1500,0,623.9606612472649]         //7: O V R
    ],[
    [0,4,7,3], // V
    [2,6,5,1], // H
    [0,3,2,1], // U
    [4,5,6,7], // O
    [0,1,5,4], // L
    [3,7,6,2]  // R
    ]);
translate([517.96, 0-1, 0]) cube([840, 304+2, 2125]);
    }
    translate([2977.96, 0-1, 0]) window(900, [1430], 1430, 304, flipy=false, flipText=true, withText=showWindowMeasures, visible=showWindows, id="B");translate([1147.96, 0-1, 0]) window(0, [660], 2125, 304, flipy=false, flipText=true, withText=showWindowMeasures, visible=showWindows, id="C");translate([457.96000000000004, 0-1, 0]) window(2600, [1500], 1230, 304, flipy=false, flipText=true, withText=showWindowMeasures, visible=showWindows, id="D");

    translate([517.96, 0-1, 0]) door(840, 2125, 304, 0, 1, visible=showDoors, id="B");

    translate([132.0,0,0]) linear_extrude(2763.4574213425894)             measurement(4975.96, 1600+400*0, true, false, visible=showMeasures);
    
    }
    module wall_housesued(showDoors, showDoorMeasures, showWindows, showWindowMeasures, showMeasures, showAreas) /*pos: [0.0,-14158.0,0]*/
/* target */ translate([0.0,302.0,0]) rotate([0,0,90])
/* origin */ translate([-14460,0,0])
    union() {
    difference() {
    union() {
    translate([0,0,0]) cube([14460, 302, 2500.0]);
    
    }
    translate([11460, 0-1, 900]) polyhedron([
    [0,0,0],                     //0: U V L
    [0,304,0],               //1: U H L
    [1830,304,0],        //2: U H R
    [1830,0,0],              //3: U V R
    [0,0,1330.0],               //4: O V L
    [0,304,1330.0],         //5: O H L
    [1830,304,1330],  //6: O H R
    [1830,0,1330]         //7: O V R
    ],[
    [0,4,7,3], // V
    [2,6,5,1], // H
    [0,3,2,1], // U
    [4,5,6,7], // O
    [0,1,5,4], // L
    [3,7,6,2]  // R
    ]);translate([8160, 0-1, 900]) polyhedron([
    [0,0,0],                     //0: U V L
    [0,304,0],               //1: U H L
    [1830,304,0],        //2: U H R
    [1830,0,0],              //3: U V R
    [0,0,1330.0],               //4: O V L
    [0,304,1330.0],         //5: O H L
    [1830,304,1330],  //6: O H R
    [1830,0,1330]         //7: O V R
    ],[
    [0,4,7,3], // V
    [2,6,5,1], // H
    [0,3,2,1], // U
    [4,5,6,7], // O
    [0,1,5,4], // L
    [3,7,6,2]  // R
    ]);translate([5460, 0-1, 900]) polyhedron([
    [0,0,0],                     //0: U V L
    [0,304,0],               //1: U H L
    [1830,304,0],        //2: U H R
    [1830,0,0],              //3: U V R
    [0,0,1330.0],               //4: O V L
    [0,304,1330.0],         //5: O H L
    [1830,304,1330],  //6: O H R
    [1830,0,1330]         //7: O V R
    ],[
    [0,4,7,3], // V
    [2,6,5,1], // H
    [0,3,2,1], // U
    [4,5,6,7], // O
    [0,1,5,4], // L
    [3,7,6,2]  // R
    ]);translate([2000, 0-1, 900]) polyhedron([
    [0,0,0],                     //0: U V L
    [0,304,0],               //1: U H L
    [1200,304,0],        //2: U H R
    [1200,0,0],              //3: U V R
    [0,0,1330.0],               //4: O V L
    [0,304,1330.0],         //5: O H L
    [1200,304,1330],  //6: O H R
    [1200,0,1330]         //7: O V R
    ],[
    [0,4,7,3], // V
    [2,6,5,1], // H
    [0,3,2,1], // U
    [4,5,6,7], // O
    [0,1,5,4], // L
    [3,7,6,2]  // R
    ]);

    }
    translate([11460, 0-1, 0]) window(900, [1830], 1330, 304, flipy=true, flipText=false, withText=showWindowMeasures, visible=showWindows, id="H");translate([8160, 0-1, 0]) window(900, [1830], 1330, 304, flipy=true, flipText=false, withText=showWindowMeasures, visible=showWindows, id="I");translate([5460, 0-1, 0]) window(900, [1830], 1330, 304, flipy=true, flipText=false, withText=showWindowMeasures, visible=showWindows, id="J");translate([2000, 0-1, 0]) window(900, [1200], 1330, 304, flipy=true, flipText=false, withText=showWindowMeasures, visible=showWindows, id="K");

    

    translate([302,0,0]) linear_extrude(2500.0)             measurement(1600, 1600+400*0, true, true, visible=showMeasures);translate([302,0,0]) linear_extrude(2500.0)             measurement(3684, 1600+400*1, true, true, visible=showMeasures);translate([4170,0,0]) linear_extrude(2500.0)             measurement(3700, 1600+400*0, true, true, visible=showMeasures);translate([8054,0,0]) linear_extrude(2500.0)             measurement(6104, 1600+400*0, true, true, visible=showMeasures);translate([0,0,0]) linear_extrude(2500.0)             measurement(14460, 1600+400*2, true, true, visible=showMeasures);
    translate([1700,-3500,0]) linear_extrude(2500.0) roomArea("WC", 2.56, true, visible=showAreas);translate([3000,-1800,0]) linear_extrude(2500.0) roomArea("Bad", 4.479744, true, visible=showAreas);translate([7000,-1800,0]) linear_extrude(2500.0) roomArea("Schlafzimmer", 14.4892, true, visible=showAreas);translate([12500,-2500,0]) linear_extrude(2500.0) roomArea("Wohnzimmer", 34.35355616, true, visible=showAreas);translate([12500,-8500,0]) linear_extrude(2500.0) roomArea("Küche/Esszimmer", 22.90931984, true, visible=showAreas);translate([8300,-8500,0]) linear_extrude(2500.0) roomArea("Kinderzimmer1", 14.430284, true, visible=showAreas);translate([5200,-8500,0]) linear_extrude(2500.0) roomArea("Kinderzimmer2", 14.430284, true, visible=showAreas);translate([2100,-8500,0]) linear_extrude(2500.0) roomArea("Kinderzimmer3", 14.430284, true, visible=showAreas);translate([2100,-5500,0]) linear_extrude(2500.0) roomArea("Flur", 16.847110079999997, true, visible=showAreas);translate([15460,-11868.0,0]) linear_extrude(2500.0) roomArea("Nettofläche", 138.92978208, true, visible=showAreas);
    }
    module wall_houseost(showDoors, showDoorMeasures, showWindows, showWindowMeasures, showMeasures, showAreas) /*pos: [5760.04,-13856.0,0]*/
/* target */ translate([0.0,-14158.0,0]) rotate([0,0,180])
/* origin */ translate([-5760.04,-302,0])
    union() {
    difference() {
    union() {
    polyhedron([
    [0,0,0],                     //0: U V L
    [0,302,0],               //1: U H L
    [5760.04,302,0],        //2: U H R
    [5760.04,0,0],              //3: U V R
    [0,0,4827.207221859537],               //4: O V L
    [0,302,4827.207221859537],         //5: O H L
    [5760.04,302,2500.0],  //6: O H R
    [5760.04,0,2500.0]         //7: O V R
    ],[
    [0,4,7,3], // V
    [2,6,5,1], // H
    [0,3,2,1], // U
    [4,5,6,7], // O
    [0,1,5,4], // L
    [3,7,6,2]  // R
    ]);
    
    }
    translate([200, 0-1, 2600]) polyhedron([
    [0,0,0],                     //0: U V L
    [0,304,0],               //1: U H L
    [1130,304,0],        //2: U H R
    [1130,0,0],              //3: U V R
    [0,0,1230],               //4: O V L
    [0,304,1230],         //5: O H L
    [1130,304,773.4503648062728],  //6: O H R
    [1130,0,773.4503648062728]         //7: O V R
    ],[
    [0,4,7,3], // V
    [2,6,5,1], // H
    [0,3,2,1], // U
    [4,5,6,7], // O
    [0,1,5,4], // L
    [3,7,6,2]  // R
    ]);translate([3200, 0-1, 1000]) polyhedron([
    [0,0,0],                     //0: U V L
    [0,304,0],               //1: U H L
    [630,304,0],        //2: U H R
    [630,0,0],              //3: U V R
    [0,0,1030.0],               //4: O V L
    [0,304,1030.0],         //5: O H L
    [630,304,1030],  //6: O H R
    [630,0,1030]         //7: O V R
    ],[
    [0,4,7,3], // V
    [2,6,5,1], // H
    [0,3,2,1], // U
    [4,5,6,7], // O
    [0,1,5,4], // L
    [3,7,6,2]  // R
    ]);
translate([900, 0-1, 0]) cube([1130, 304+2, 2230]);
    }
    translate([200, 0-1, 0]) window(2600, [1130], 1230, 304, flipy=true, flipText=false, withText=showWindowMeasures, visible=showWindows, id="L");translate([3200, 0-1, 0]) window(1000, [630], 1030, 304, flipy=true, flipText=false, withText=showWindowMeasures, visible=showWindows, id="M");

    translate([900, 0-1, 0]) door(1130, 2230, 304, 1, 1, visible=showDoors, id="C");

    translate([4544.04,0,0]) linear_extrude(2500.0)             measurement(1216, 1600+400*0, true, true, visible=showMeasures);translate([2760.04,0,0]) linear_extrude(2500.0)             measurement(3000, 1600+400*1, true, true, visible=showMeasures);translate([2760.04,0,0]) linear_extrude(2500.0)             measurement(1600, 1600+400*0, true, true, visible=showMeasures);translate([132.0,0,0]) linear_extrude(2500.0)             measurement(2444.04, 1600+400*0, true, true, visible=showMeasures);translate([0,0,0]) linear_extrude(2500.0)             measurement(5760.04, 1600+400*2, true, true, visible=showMeasures);
    
    }
    module wall_houseost2(showDoors, showDoorMeasures, showWindows, showWindowMeasures, showMeasures, showAreas) /*pos: [10868.0,-13856.0,0]*/
/* target */ translate([5760.04,-13856.0,0]) rotate([0,0,180])
/* origin */ translate([-5107.96,0,0])
    union() {
    difference() {
    union() {
    polyhedron([
    [0,0,0],                     //0: U V L
    [0,302,0],               //1: U H L
    [5107.96,302,0],        //2: U H R
    [5107.96,0,0],              //3: U V R
    [0,0,2763.4574213425894],               //4: O V L
    [0,302,2763.4574213425894],         //5: O H L
    [5107.96,302,4827.207221859537],  //6: O H R
    [5107.96,0,4827.207221859537]         //7: O V R
    ],[
    [0,4,7,3], // V
    [2,6,5,1], // H
    [0,3,2,1], // U
    [4,5,6,7], // O
    [0,1,5,4], // L
    [3,7,6,2]  // R
    ]);
    
    }
    translate([1057.96, 0-1, 900]) polyhedron([
    [0,0,0],                     //0: U V L
    [0,304,0],               //1: U H L
    [1430,304,0],        //2: U H R
    [1430,0,0],              //3: U V R
    [0,0,1430.0],               //4: O V L
    [0,304,1430.0],         //5: O H L
    [1430,304,1430],  //6: O H R
    [1430,0,1430]         //7: O V R
    ],[
    [0,4,7,3], // V
    [2,6,5,1], // H
    [0,3,2,1], // U
    [4,5,6,7], // O
    [0,1,5,4], // L
    [3,7,6,2]  // R
    ]);

    }
    translate([1057.96, 0-1, 0]) window(900, [1430], 1430, 304, flipy=true, flipText=false, withText=showWindowMeasures, visible=showWindows, id="N");

    

    translate([0,0,0]) linear_extrude(4827.207221859537)             measurement(5107.96, 1600+400*2, true, true, visible=showMeasures);translate([-302,0,0]) linear_extrude(4827.207221859537)             measurement(11472.0, 1600+400*3, true, true, visible=showMeasures);
    
    }
    module wall_housenord(showDoors, showDoorMeasures, showWindows, showWindowMeasures, showMeasures, showAreas) /*pos: [10868.0,302.0,0]*/
/* target */ translate([10868.0,-14158.0,0]) rotate([0,0,270])
/* origin */ translate([-14460,0,0])
    union() {
    difference() {
    union() {
    translate([0,0,0]) cube([14460, 302, 2763.4574213425894]);
    
    }
    translate([11460, 0-1, 900]) polyhedron([
    [0,0,0],                     //0: U V L
    [0,304,0],               //1: U H L
    [1830,304,0],        //2: U H R
    [1830,0,0],              //3: U V R
    [0,0,1430.0],               //4: O V L
    [0,304,1430.0],         //5: O H L
    [1830,304,1430],  //6: O H R
    [1830,0,1430]         //7: O V R
    ],[
    [0,4,7,3], // V
    [2,6,5,1], // H
    [0,3,2,1], // U
    [4,5,6,7], // O
    [0,1,5,4], // L
    [3,7,6,2]  // R
    ]);translate([8460, 0-1, 900]) polyhedron([
    [0,0,0],                     //0: U V L
    [0,304,0],               //1: U H L
    [1830,304,0],        //2: U H R
    [1830,0,0],              //3: U V R
    [0,0,1430.0],               //4: O V L
    [0,304,1430.0],         //5: O H L
    [1830,304,1430],  //6: O H R
    [1830,0,1430]         //7: O V R
    ],[
    [0,4,7,3], // V
    [2,6,5,1], // H
    [0,3,2,1], // U
    [4,5,6,7], // O
    [0,1,5,4], // L
    [3,7,6,2]  // R
    ]);translate([5460, 0-1, 900]) polyhedron([
    [0,0,0],                     //0: U V L
    [0,304,0],               //1: U H L
    [1830,304,0],        //2: U H R
    [1830,0,0],              //3: U V R
    [0,0,1430.0],               //4: O V L
    [0,304,1430.0],         //5: O H L
    [1830,304,1430],  //6: O H R
    [1830,0,1430]         //7: O V R
    ],[
    [0,4,7,3], // V
    [2,6,5,1], // H
    [0,3,2,1], // U
    [4,5,6,7], // O
    [0,1,5,4], // L
    [3,7,6,2]  // R
    ]);translate([3560, 0-1, 900]) polyhedron([
    [0,0,0],                     //0: U V L
    [0,304,0],               //1: U H L
    [1430,304,0],        //2: U H R
    [1430,0,0],              //3: U V R
    [0,0,1430.0],               //4: O V L
    [0,304,1430.0],         //5: O H L
    [1430,304,1430],  //6: O H R
    [1430,0,1430]         //7: O V R
    ],[
    [0,4,7,3], // V
    [2,6,5,1], // H
    [0,3,2,1], // U
    [4,5,6,7], // O
    [0,1,5,4], // L
    [3,7,6,2]  // R
    ]);translate([600, 0-1, 900]) polyhedron([
    [0,0,0],                     //0: U V L
    [0,304,0],               //1: U H L
    [1430,304,0],        //2: U H R
    [1430,0,0],              //3: U V R
    [0,0,1430.0],               //4: O V L
    [0,304,1430.0],         //5: O H L
    [1430,304,1430],  //6: O H R
    [1430,0,1430]         //7: O V R
    ],[
    [0,4,7,3], // V
    [2,6,5,1], // H
    [0,3,2,1], // U
    [4,5,6,7], // O
    [0,1,5,4], // L
    [3,7,6,2]  // R
    ]);

    }
    translate([11460, 0-1, 0]) window(900, [1830], 1430, 304, flipy=false, flipText=true, withText=showWindowMeasures, visible=showWindows, id="O");translate([8460, 0-1, 0]) window(900, [1830], 1430, 304, flipy=false, flipText=true, withText=showWindowMeasures, visible=showWindows, id="P");translate([5460, 0-1, 0]) window(900, [1830], 1430, 304, flipy=false, flipText=true, withText=showWindowMeasures, visible=showWindows, id="Q");translate([3560, 0-1, 0]) window(900, [1430], 1430, 304, flipy=false, flipText=true, withText=showWindowMeasures, visible=showWindows, id="R");translate([600, 0-1, 0]) window(900, [1430], 1430, 304, flipy=false, flipText=true, withText=showWindowMeasures, visible=showWindows, id="S");

    

    translate([302,0,0]) linear_extrude(2763.4574213425894)             measurement(4604, 1600+400*0, true, false, visible=showMeasures);translate([5090,0,0]) linear_extrude(2763.4574213425894)             measurement(2900, 1600+400*0, true, false, visible=showMeasures);translate([8174,0,0]) linear_extrude(2763.4574213425894)             measurement(2900, 1600+400*0, true, false, visible=showMeasures);translate([11258,0,0]) linear_extrude(2763.4574213425894)             measurement(2900, 1600+400*0, true, false, visible=showMeasures);translate([0,0,0]) linear_extrude(2763.4574213425894)             measurement(14460, 1600+400*1, true, false, visible=showMeasures);
    
    }
    module wall_housekinder1(showDoors, showDoorMeasures, showWindows, showWindowMeasures, showMeasures, showAreas) /*pos: [5628.04,-4604.0,0]*/
/* target */ translate([5628.04,-13856.0,0]) rotate([0,0,270])
/* origin */ translate([-9252,0,0])
    union() {
    difference() {
    union() {
    translate([0,0,0]) cube([9252, 264, 2500.0]);
    
    }
    
translate([384, 0-1, 0]) cube([885, 266+2, 2125]);translate([6483, 0-1, 0]) cube([885, 266+2, 2125]);
    }
    

    translate([384, 0-1, 0]) door(885, 2125, 266, 0, 1, visible=showDoors, id="D");translate([6483, 0-1, 0]) door(885, 2125, 266, 0, 1, visible=showDoors, id="E");

    
    
    }
    module wall_housekinder2(showDoors, showDoorMeasures, showWindows, showWindowMeasures, showMeasures, showAreas) /*pos: [5892.04,-10956.0,0]*/
/* target */ translate([5892.04,-10956.0,0]) rotate([0,0,360])
/* origin */ translate([0,0,0])
    union() {
    difference() {
    union() {
    translate([0,0,0]) cube([4975.96, 184, 2500.0]);
    
    }
    

    }
    

    

    
    
    }
    module wall_housekinder3(showDoors, showDoorMeasures, showWindows, showWindowMeasures, showMeasures, showAreas) /*pos: [5892.04,-7872.0,0]*/
/* target */ translate([5892.04,-7872.0,0]) rotate([0,0,360])
/* origin */ translate([0,0,0])
    union() {
    difference() {
    union() {
    translate([0,0,0]) cube([4975.96, 184, 2500.0]);
    
    }
    

    }
    

    

    
    
    }
    module wall_housekinder4(showDoors, showDoorMeasures, showWindows, showWindowMeasures, showMeasures, showAreas) /*pos: [5892.04,-4788.0,0]*/
/* target */ translate([5892.04,-4788.0,0]) rotate([0,0,360])
/* origin */ translate([0,0,0])
    union() {
    difference() {
    union() {
    translate([0,0,0]) cube([4975.96, 184, 2500.0]);
    
    }
    

    }
    

    

    
    
    }
    module wall_housebad1(showDoors, showDoorMeasures, showWindows, showWindowMeasures, showMeasures, showAreas) /*pos: [3000.0,-10172.0,0]*/
/* target */ translate([3000.0,-13856.0,0]) rotate([0,0,270])
/* origin */ translate([-3684,0,0])
    union() {
    difference() {
    union() {
    translate([0,0,0]) cube([3684, 184, 2500.0]);
    
    }
    
translate([900, 0-1, 0]) cube([885, 186+2, 2125]);translate([2134, 0-1, 0]) cube([725, 186+2, 2125]);
    }
    

    translate([900, 0-1, 0]) door(885, 2125, 186, 1, 1, visible=showDoors, id="F");translate([2134, 0-1, 0]) door(725, 2125, 186, 0, 1, visible=showDoors, id="G");

    
    
    }
    module wall_housebad2(showDoors, showDoorMeasures, showWindows, showWindowMeasures, showMeasures, showAreas) /*pos: [0.0,-10172.0,0]*/
/* target */ translate([3184.0,-10172.0,0]) rotate([0,0,360])
/* origin */ translate([-3184,0,0])
    union() {
    difference() {
    union() {
    translate([0,0,0]) cube([3184, 184, 2500.0]);
    
    }
    

    }
    

    

    
    
    }
    module wall_housebad3(showDoors, showDoorMeasures, showWindows, showWindowMeasures, showMeasures, showAreas) /*pos: [1400.0,-12256.0,0]*/
/* target */ translate([3000.0,-12072.0,0]) rotate([0,0,360])
/* origin */ translate([-1600,-184,0])
    union() {
    difference() {
    union() {
    translate([0,0,0]) cube([1600, 184, 2500.0]);
    
    }
    

    }
    

    

    
    
    }
    module wall_housebad4(showDoors, showDoorMeasures, showWindows, showWindowMeasures, showMeasures, showAreas) /*pos: [1400.0,-13856.0,0]*/
/* target */ translate([1400.0,-12072.0,0]) rotate([0,0,450])
/* origin */ translate([-1784,0,0])
    union() {
    difference() {
    union() {
    translate([0,0,0]) cube([1784, 184, 2500.0]);
    
    }
    

    }
    

    

    
    
    }
    module wall_houseschlafen1(showDoors, showDoorMeasures, showWindows, showWindowMeasures, showMeasures, showAreas) /*pos: [3184.0,-10172.0,0]*/
/* target */ translate([3184.0,-10172.0,0]) rotate([0,0,360])
/* origin */ translate([0,0,0])
    union() {
    difference() {
    union() {
    translate([0,0,0]) cube([916, 184, 2500.0]);
    
    }
    

    }
    

    

    
    
    }
    module wall_houseschlafen2(showDoors, showDoorMeasures, showWindows, showWindowMeasures, showMeasures, showAreas) /*pos: [4100.0,-9988.0,0]*/
/* target */ translate([4100.0,-9988.0,0]) rotate([0,0,450])
/* origin */ translate([0,0,0])
    union() {
    difference() {
    union() {
    translate([0,0,0]) cube([3700, 184, 2500.0]);
    
    }
    
translate([100, 0-1, 0]) cube([885, 186+2, 2125]);
    }
    

    translate([100, 0-1, 0]) door(885, 2125, 186, 0, 0, visible=showDoors, id="H");

    
    
    }
    module wall_houseschlafen3(showDoors, showDoorMeasures, showWindows, showWindowMeasures, showMeasures, showAreas) /*pos: [4100.0,-6104.0,0]*/
/* target */ translate([4100.0,-6288.0,0]) rotate([0,0,540])
/* origin */ translate([0,-184,0])
    union() {
    difference() {
    union() {
    translate([0,0,0]) cube([4100, 184, 2500.0]);
    
    }
    

    }
    

    

    
    
    }
    
module house(showDoors=false, showDoorMeasures=false, showWindows=false, showWindowMeasures=false, showMeasures=false, showAreas=false) {
 wall_housewest(showDoors, showDoorMeasures, showWindows, showWindowMeasures, showMeasures, showAreas);
 wall_housewest2(showDoors, showDoorMeasures, showWindows, showWindowMeasures, showMeasures, showAreas);
 wall_housesued(showDoors, showDoorMeasures, showWindows, showWindowMeasures, showMeasures, showAreas);
 wall_houseost(showDoors, showDoorMeasures, showWindows, showWindowMeasures, showMeasures, showAreas);
 wall_houseost2(showDoors, showDoorMeasures, showWindows, showWindowMeasures, showMeasures, showAreas);
 wall_housenord(showDoors, showDoorMeasures, showWindows, showWindowMeasures, showMeasures, showAreas);
 wall_housekinder1(showDoors, showDoorMeasures, showWindows, showWindowMeasures, showMeasures, showAreas);
 wall_housekinder2(showDoors, showDoorMeasures, showWindows, showWindowMeasures, showMeasures, showAreas);
 wall_housekinder3(showDoors, showDoorMeasures, showWindows, showWindowMeasures, showMeasures, showAreas);
 wall_housekinder4(showDoors, showDoorMeasures, showWindows, showWindowMeasures, showMeasures, showAreas);
 wall_housebad1(showDoors, showDoorMeasures, showWindows, showWindowMeasures, showMeasures, showAreas);
 wall_housebad2(showDoors, showDoorMeasures, showWindows, showWindowMeasures, showMeasures, showAreas);
 wall_housebad3(showDoors, showDoorMeasures, showWindows, showWindowMeasures, showMeasures, showAreas);
 wall_housebad4(showDoors, showDoorMeasures, showWindows, showWindowMeasures, showMeasures, showAreas);
 wall_houseschlafen1(showDoors, showDoorMeasures, showWindows, showWindowMeasures, showMeasures, showAreas);
 wall_houseschlafen2(showDoors, showDoorMeasures, showWindows, showWindowMeasures, showMeasures, showAreas);
 wall_houseschlafen3(showDoors, showDoorMeasures, showWindows, showWindowMeasures, showMeasures, showAreas);
}//Bodenplatte
module bodenplatte() {
 translate([-302,-14158,-200]) difference() { cube([11472.0,14460,--200]);
translate([4002,2086,-1]) cube([854,3000,--200+2]);translate([1502,302,-1]) cube([184,184,--200+2]);translate([10986.0,9434,-1]) cube([184,184,--200+2]);translate([302,5270,-1]) cube([184,600,--200+2]);}}//Veranda
module veranda() {
translate([-302,302,-200]) cube([11472.0,2500,--200]);
translate([11170.0,-3698,-200]) cube([4500,6500,--200]);
}

module wall_basementwest(showDoors, showDoorMeasures, showWindows, showWindowMeasures, showMeasures, showAreas) 
    union() {
    difference() {
    union() {
    translate([0,0,0]) cube([10628, 302, 2560.0]);
    
    }
    translate([4000, 0-1, 1500]) polyhedron([
    [0,0,0],                     //0: U V L
    [0,304,0],               //1: U H L
    [1230,304,0],        //2: U H R
    [1230,0,0],              //3: U V R
    [0,0,830.0],               //4: O V L
    [0,304,830.0],         //5: O H L
    [1230,304,830],  //6: O H R
    [1230,0,830]         //7: O V R
    ],[
    [0,4,7,3], // V
    [2,6,5,1], // H
    [0,3,2,1], // U
    [4,5,6,7], // O
    [0,1,5,4], // L
    [3,7,6,2]  // R
    ]);

    }
    translate([4000, 0-1, 0]) window(1500, [1230], 830, 304, flipy=true, flipText=false, withText=showWindowMeasures, visible=showWindows, id="B");

    

    translate([0,0,0]) linear_extrude(2560.0)             measurement(3400, 1600+400*0, true, true, visible=showMeasures);translate([3584,0,0]) linear_extrude(2560.0)             measurement(1418, 1600+400*0, true, true, visible=showMeasures);translate([5002,0,0]) linear_extrude(2560.0)             measurement(5626, 1600+400*0, true, true, visible=showMeasures);
    
    }
    module wall_basementsued(showDoors, showDoorMeasures, showWindows, showWindowMeasures, showMeasures, showAreas) /*pos: [0.0,-13918.0,0]*/
/* target */ translate([0.0,302.0,0]) rotate([0,0,90])
/* origin */ translate([-14220,0,0])
    union() {
    difference() {
    union() {
    translate([0,0,0]) cube([14220, 302, 2560.0]);
    
    }
    translate([7900, 0-1, 1500]) polyhedron([
    [0,0,0],                     //0: U V L
    [0,304,0],               //1: U H L
    [1430,304,0],        //2: U H R
    [1430,0,0],              //3: U V R
    [0,0,830.0],               //4: O V L
    [0,304,830.0],         //5: O H L
    [1430,304,830],  //6: O H R
    [1430,0,830]         //7: O V R
    ],[
    [0,4,7,3], // V
    [2,6,5,1], // H
    [0,3,2,1], // U
    [4,5,6,7], // O
    [0,1,5,4], // L
    [3,7,6,2]  // R
    ]);translate([11800, 0-1, 1500]) polyhedron([
    [0,0,0],                     //0: U V L
    [0,304,0],               //1: U H L
    [1230,304,0],        //2: U H R
    [1230,0,0],              //3: U V R
    [0,0,830.0],               //4: O V L
    [0,304,830.0],         //5: O H L
    [1230,304,830],  //6: O H R
    [1230,0,830]         //7: O V R
    ],[
    [0,4,7,3], // V
    [2,6,5,1], // H
    [0,3,2,1], // U
    [4,5,6,7], // O
    [0,1,5,4], // L
    [3,7,6,2]  // R
    ]);
translate([550, 0-1, 0]) cube([3500, 304+2, 2125]);
    }
    translate([7900, 0-1, 0]) window(1500, [1430], 830, 304, flipy=true, flipText=false, withText=showWindowMeasures, visible=showWindows, id="C");translate([11800, 0-1, 0]) window(1500, [1230], 830, 304, flipy=true, flipText=false, withText=showWindowMeasures, visible=showWindows, id="D");

    translate([550, 0-1, 0]) garageDoor(3500, 2125, 304, 0, 1, visible=showDoors);

    translate([302,0,0]) linear_extrude(2560.0)             measurement(4700, 1600+400*0, true, true, visible=showMeasures);translate([5186,0,0]) linear_extrude(2560.0)             measurement(4448, 1600+400*0, true, true, visible=showMeasures);translate([9818,0,0]) linear_extrude(2560.0)             measurement(4100, 1600+400*0, true, true, visible=showMeasures);translate([0,0,0]) linear_extrude(2560.0)             measurement(14220, 1600+400*1, true, true, visible=showMeasures);
    translate([2652.0,-2800.0,0]) linear_extrude(2560.0) roomArea("Garage", 25.4552, true, visible=showAreas);translate([7410.0,-2501.0,0]) linear_extrude(2560.0) roomArea("Treppe,HWR", 21.430464, true, visible=showAreas);translate([11684.0,-1750.0,0]) linear_extrude(2560.0) roomArea("Brauerei", 13.94, true, visible=showAreas);translate([11168.0,-7500.0,0]) linear_extrude(2560.0) roomArea("Hobby", 28.8804, true, visible=showAreas);translate([3594.0,-8000.0,0]) linear_extrude(2560.0) roomArea("Werkstatt", 49.5812, true, visible=showAreas);translate([15220,-11628,0]) linear_extrude(2560.0) roomArea("Nettofläche", 139.287264, true, visible=showAreas);
    }
    module wall_basementost(showDoors, showDoorMeasures, showWindows, showWindowMeasures, showMeasures, showAreas) /*pos: [10628.0,-13616.0,0]*/
/* target */ translate([0.0,-13918.0,0]) rotate([0,0,180])
/* origin */ translate([-10628,-302,0])
    union() {
    difference() {
    union() {
    translate([0,0,0]) cube([10628, 302, 2560.0]);
    
    }
    translate([8000, 0-1, 1500]) polyhedron([
    [0,0,0],                     //0: U V L
    [0,304,0],               //1: U H L
    [1230,304,0],        //2: U H R
    [1230,0,0],              //3: U V R
    [0,0,830.0],               //4: O V L
    [0,304,830.0],         //5: O H L
    [1230,304,830],  //6: O H R
    [1230,0,830]         //7: O V R
    ],[
    [0,4,7,3], // V
    [2,6,5,1], // H
    [0,3,2,1], // U
    [4,5,6,7], // O
    [0,1,5,4], // L
    [3,7,6,2]  // R
    ]);translate([2400, 0-1, 1500]) polyhedron([
    [0,0,0],                     //0: U V L
    [0,304,0],               //1: U H L
    [1230,304,0],        //2: U H R
    [1230,0,0],              //3: U V R
    [0,0,830.0],               //4: O V L
    [0,304,830.0],         //5: O H L
    [1230,304,830],  //6: O H R
    [1230,0,830]         //7: O V R
    ],[
    [0,4,7,3], // V
    [2,6,5,1], // H
    [0,3,2,1], // U
    [4,5,6,7], // O
    [0,1,5,4], // L
    [3,7,6,2]  // R
    ]);

    }
    translate([8000, 0-1, 0]) window(1500, [1230], 830, 304, flipy=true, flipText=false, withText=showWindowMeasures, visible=showWindows, id="E");translate([2400, 0-1, 0]) window(1500, [1230], 830, 304, flipy=true, flipText=false, withText=showWindowMeasures, visible=showWindows, id="F");

    

    translate([0,0,0]) linear_extrude(2560.0)             measurement(5028, 1600+400*1, true, true, visible=showMeasures);translate([5212,0,0]) linear_extrude(2560.0)             measurement(5416, 1600+400*1, true, true, visible=showMeasures);translate([5810,0,0]) linear_extrude(2560.0)             measurement(4818, 1600+400*0, true, true, visible=showMeasures);translate([-302,0,0]) linear_extrude(2560.0)             measurement(11232, 1600+400*2, true, true, visible=showMeasures);
    
    }
    module wall_basementnord(showDoors, showDoorMeasures, showWindows, showWindowMeasures, showMeasures, showAreas) /*pos: [10628.0,302.0,0]*/
/* target */ translate([10628.0,-13918.0,0]) rotate([0,0,270])
/* origin */ translate([-14220,0,0])
    union() {
    difference() {
    union() {
    translate([0,0,0]) cube([14220, 302, 2560.0]);
    
    }
    translate([3000, 0-1, 1500]) polyhedron([
    [0,0,0],                     //0: U V L
    [0,304,0],               //1: U H L
    [1230,304,0],        //2: U H R
    [1230,0,0],              //3: U V R
    [0,0,830.0],               //4: O V L
    [0,304,830.0],         //5: O H L
    [1230,304,830],  //6: O H R
    [1230,0,830]         //7: O V R
    ],[
    [0,4,7,3], // V
    [2,6,5,1], // H
    [0,3,2,1], // U
    [4,5,6,7], // O
    [0,1,5,4], // L
    [3,7,6,2]  // R
    ]);translate([5800, 0-1, 1500]) polyhedron([
    [0,0,0],                     //0: U V L
    [0,304,0],               //1: U H L
    [1230,304,0],        //2: U H R
    [1230,0,0],              //3: U V R
    [0,0,830.0],               //4: O V L
    [0,304,830.0],         //5: O H L
    [1230,304,830],  //6: O H R
    [1230,0,830]         //7: O V R
    ],[
    [0,4,7,3], // V
    [2,6,5,1], // H
    [0,3,2,1], // U
    [4,5,6,7], // O
    [0,1,5,4], // L
    [3,7,6,2]  // R
    ]);translate([10150, 0-1, 1500]) polyhedron([
    [0,0,0],                     //0: U V L
    [0,304,0],               //1: U H L
    [1230,304,0],        //2: U H R
    [1230,0,0],              //3: U V R
    [0,0,830.0],               //4: O V L
    [0,304,830.0],         //5: O H L
    [1230,304,830],  //6: O H R
    [1230,0,830]         //7: O V R
    ],[
    [0,4,7,3], // V
    [2,6,5,1], // H
    [0,3,2,1], // U
    [4,5,6,7], // O
    [0,1,5,4], // L
    [3,7,6,2]  // R
    ]);

    }
    translate([3000, 0-1, 0]) window(1500, [1230], 830, 304, flipy=false, flipText=true, withText=showWindowMeasures, visible=showWindows, id="G");translate([5800, 0-1, 0]) window(1500, [1230], 830, 304, flipy=false, flipText=true, withText=showWindowMeasures, visible=showWindows, id="H");translate([10150, 0-1, 0]) window(1500, [1230], 830, 304, flipy=false, flipText=true, withText=showWindowMeasures, visible=showWindows, id="I");

    

    translate([302,0,0]) linear_extrude(2560.0)             measurement(4100, 1600+400*0, true, false, visible=showMeasures);translate([4586,0,0]) linear_extrude(2560.0)             measurement(4448, 1600+400*0, true, false, visible=showMeasures);translate([4586,0,0]) linear_extrude(2560.0)             measurement(9332, 1600+400*1, true, false, visible=showMeasures);
    
    }
    module wall_basementgarage(showDoors, showDoorMeasures, showWindows, showWindowMeasures, showMeasures, showAreas) /*pos: [5600.0,-8732.0,0]*/
/* target */ translate([0.0,-8732.0,0]) rotate([0,0,180])
/* origin */ translate([-5600,0,0])
    union() {
    difference() {
    union() {
    translate([0,0,0]) cube([5600, 184, 2560.0]);
    
    }
    
translate([4000, 0-1, 0]) cube([985, 186+2, 2125]);
    }
    

    translate([4000, 0-1, 0]) door(985, 2125, 186, 1, 1, visible=showDoors, id="C");

    
    
    }
    module wall_basementgarage2(showDoors, showDoorMeasures, showWindows, showWindowMeasures, showMeasures, showAreas) /*pos: [5416.0,-8916.0,0]*/
/* target */ translate([5600.0,-8916.0,0]) rotate([0,0,270])
/* origin */ translate([0,-184,0])
    union() {
    difference() {
    union() {
    translate([0,0,0]) cube([4700, 184, 2560.0]);
    
    }
    
translate([200, 0-1, 0]) cube([2500, 186+2, 2125]);
    }
    

    translate([200, 0-1, 0]) workshopDoor(2500, 2125, 186, 0, 0, visible=showDoors);

    
    
    }
    module wall_basementbrauGast(showDoors, showDoorMeasures, showWindows, showWindowMeasures, showMeasures, showAreas) /*pos: [5002.0,-4100.0,0]*/
/* target */ translate([0.0,-4100.0,0]) rotate([0,0,180])
/* origin */ translate([-5002,0,0])
    union() {
    difference() {
    union() {
    translate([0,0,0]) cube([5002, 184, 2560.0]);
    
    }
    
translate([1700, 0-1, 0]) cube([985, 186+2, 2125]);translate([250, 0-1, 0]) cube([985, 186+2, 2125]);
    }
    

    translate([1700, 0-1, 0]) door(985, 2125, 186, 0, 0, visible=showDoors, id="E");translate([250, 0-1, 0]) door(985, 2125, 186, 1, 1, visible=showDoors, id="F");

    
    
    }
    module wall_basementbrauGast2(showDoors, showDoorMeasures, showWindows, showWindowMeasures, showMeasures, showAreas) /*pos: [3400.0,0.0,0]*/
/* target */ translate([3400.0,-4100.0,0]) rotate([0,0,270])
/* origin */ translate([-4100,0,0])
    union() {
    difference() {
    union() {
    translate([0,0,0]) cube([4100, 184, 2560.0]);
    
    }
    

    }
    

    

    
    
    }
    module wall_basementwerkstatt(showDoors, showDoorMeasures, showWindows, showWindowMeasures, showMeasures, showAreas) /*pos: [4818.0,-4284.0,0]*/
/* target */ translate([5002.0,-8732.0,0]) rotate([0,0,270])
/* origin */ translate([-4448,-184,0])
    union() {
    difference() {
    union() {
    translate([0,0,0]) cube([4448, 184, 2560.0]);
    
    }
    
translate([150, 0-1, 0]) cube([985, 186+2, 2125]);
    }
    

    translate([150, 0-1, 0]) door(985, 2125, 186, 0, 0, visible=showDoors, id="G");

    
    
    }
    module wall_basementwerkstatt2(showDoors, showDoorMeasures, showWindows, showWindowMeasures, showMeasures, showAreas) /*pos: [5002.0,-4284.0,0]*/
/* target */ translate([5002.0,-4100.0,0]) rotate([0,0,360])
/* origin */ translate([0,-184,0])
    union() {
    difference() {
    union() {
    translate([0,0,0]) cube([5626, 184, 2560.0]);
    
    }
    

    }
    

    

    
    
    }
    module basement(showDoors=false, showDoorMeasures=false, showWindows=false, showWindowMeasures=false, showMeasures=false, showAreas=false) {
color("yellow") wall_basementwest(showDoors, showDoorMeasures, showWindows, showWindowMeasures, showMeasures, showAreas);
color("green") wall_basementsued(showDoors, showDoorMeasures, showWindows, showWindowMeasures, showMeasures, showAreas);
color("red") wall_basementost(showDoors, showDoorMeasures, showWindows, showWindowMeasures, showMeasures, showAreas);
color("blue") wall_basementnord(showDoors, showDoorMeasures, showWindows, showWindowMeasures, showMeasures, showAreas);
color("purple") wall_basementgarage(showDoors, showDoorMeasures, showWindows, showWindowMeasures, showMeasures, showAreas);
color("orange") wall_basementgarage2(showDoors, showDoorMeasures, showWindows, showWindowMeasures, showMeasures, showAreas);
color("cyan") wall_basementbrauGast(showDoors, showDoorMeasures, showWindows, showWindowMeasures, showMeasures, showAreas);
color("grey") wall_basementbrauGast2(showDoors, showDoorMeasures, showWindows, showWindowMeasures, showMeasures, showAreas);
color("orange") wall_basementwerkstatt(showDoors, showDoorMeasures, showWindows, showWindowMeasures, showMeasures, showAreas);
color("yellow") wall_basementwerkstatt2(showDoors, showDoorMeasures, showWindows, showWindowMeasures, showMeasures, showAreas);
}//Bodenplatte
module bodenplatte_bm() {
 translate([-302,-13918,-200]) cube([11232,14220,--200]);
}