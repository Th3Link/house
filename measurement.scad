use <isocpeur.ttf>

r = 60;
textSize = 300;
module measurement(l,y,flip=false, flipText=false, visible=true) {
if (visible) {
translate([(flip? 0 : l),0,0]) mirror([(flip? 0 : 1),0,0]) mirror([0,(flip? 0 : 1),0]) union() {
projection(true) translate([0,y,0]) rotate([0,90,0]) union() {
translate([0,0,3*r]) mirror([0,0,1]) cylinder(r1=r, r2=0, h=3*r);
translate([0,0,3*r]) cylinder(r=1, h=l-2*3*r);
translate([0,0,l-3*r]) cylinder(r1=r, r2=0, h=3*r);
}
FT = (flipText != !flip);
translate([l/2,(FT ? -1 : 1)*r+y,0]) mirror([(FT ? 1 : 0),0,0]) mirror([0,(FT ? 1 : 0),0]) offset(delta=-textSize/30) text(str(round(l/10)/100) , size=textSize, halign="center", font="isocpeur");
projection(true) translate([0,(y < 0 ? y-r : 0),0]) union() {
    rotate([-90,0,0]) cylinder(r=1, h=abs(y)+r);
    translate([l,0,0]) rotate([-90,0,0]) cylinder(r=1, h=abs(y)+r);
}
}
}
}
measurement(1010,-100, flip=false, flipText=true);