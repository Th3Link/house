use <house.scad>
module complete(uncombined=1, poles=1, roof=1, roofCut=1, roofShadow=0, showDoors=false, 
    showDoorMeasures=false, 
    showWindows=false,
    showWindowMeasures=false,
    showMeasures=false,
    showAreas=false,
    showVeranda=false,
    showBodenplatte=false)
{
difference() {
union() {
    house(showDoors, showDoorMeasures, showWindows, showWindowMeasures, showMeasures, showAreas);
    if (showVeranda) {
        veranda();
    }
    if (showBodenplatte) {
        bodenplatte();
    }
    //poles
    if (poles) {
    translate([5670,-3600,-2800]) cube([250,250,7700]);
    translate([5670,2300,-2800]) cube([250,250,7700]);
    translate([-220,2300,-2800]) cube([250,250,6000]);
    translate([10900,2300,-2800]) cube([250,250,6000]);
    }
}
if (roofShadow) {
translate([-1100,-16700,2100-1]) cube([13750,19500,3010]);
}
if (roofCut) {
translate([200,0,2585]) rotate([0,-22,0]) translate([-1240,-16700,0]) cube([14670/2,19500,4000]);
translate([11200,0,2630]) mirror([1,0,0]) rotate([0,-22,0]) translate([-1320,-16700,0]) cube([16670/2,19500,4000]);
}
}
//roof
if (roof) {
translate([200,0,2585]) rotate([0,-22,0]) translate([-1240,-16700,0]) cube([14670/2,19500,300]);
translate([11200,0,2630]) mirror([1,0,0]) rotate([0,-22,0]) translate([-1320,-16700,0]) cube([14670/2,19500,300]);
}
if (roofShadow) {
translate([-1100,-16700,2100]) cube([13750,19500,3010]);
}

//ground
translate([120,-120,-2760*uncombined]) union() {basement(showDoors, showDoorMeasures, showWindows, showWindowMeasures, showMeasures, showAreas);
if (showBodenplatte) {
    bodenplatte_bm();
}
}
translate([0,0,-1000]) difference() {
translate([-1500,-17500,-200]) cube([14000,22000,1]);
translate([-330,-1650,-200]) cube([11750,15990,1]);
}
}
translate([250,-250,0]) complete(roofShadow=0, roof=0, showBodenplatte=1,showVeranda=1);

*projection(true) translate([250,-250,-1500]) complete(0);

*projection(true) translate([0,0,-200]) rotate([90,0,0])  complete();


*house(showDoors, showDoorMeasures, showWindows, showWindowMeasures, showMeasures, showAreas);
*translate([0,0,-2600]) basement(showDoors, showDoorMeasures, showWindows, showWindowMeasures, showMeasures, showAreas);
*translate([0,-8100,2500]) cube([4700,3500,200]);
