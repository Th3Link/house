use <module_complete.scad>
//Keller mit Fenstern
projection(true) translate([0,0,800])  complete(showDoors=true, 
    showDoorMeasures=true, 
    showWindows=true,
    showWindowMeasures=true,
    showMeasures=true,
    showAreas=true);
