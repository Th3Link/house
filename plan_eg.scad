use <module_complete.scad>
//Erdgeschoss mit Fenstern
projection(true) translate([0,0,-1500])  complete(showDoors=true, 
    showDoorMeasures=true, 
    showWindows=true,
    showWindowMeasures=true,
    showMeasures=true,
    showAreas=true);
