#!/bin/bash
mkdir -p plans
for f in plan_*.scad; do
    openscad -o plans/${f/scad/dxf} $f;
done

for f in print_*.scad; do
    openscad -o plans/${f/scad/stl} $f;
done
