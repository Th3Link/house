use <module_complete.scad>
//print EG
*intersection() {
complete(roofShadow=0, roof=0, showBodenplatte=1,showVeranda=1);
translate([-1000,-17000,-200+1]) cube([20000,22000,6000]);
}

module house()
{
intersection() {
complete(roofShadow=0, roof=0, showBodenplatte=0,showVeranda=0, showPoles=0);
translate([-1000,-17000,-200+1]) cube([20000,22000,6000]);
}
}

house();