use <house.scad>

//Erdgeschoss mit Fenstern
*projection(true) translate([0,0,-1300])  house();

//Erdgeschoss nur Türen
*projection(true) translate([0,0,-300])  house();

//Keller nur Türen
*projection(true) translate([0,0,-300])  basement();

//Keller mit Fenstern
*projection(true) translate([0,0,-2050])  basement();

//Ostansicht
*projection(true) translate([0,0,15420]) rotate([90,0,0]) complete();

//Westansicht
*projection(true)  translate([0,0,-10]) rotate([90,0,0]) complete();

//Südansicht
*projection(true) translate([0,0,-10]) rotate([0,90,0]) complete();

//Nordansicht
*projection(true) translate([0,0,11000]) rotate([0,90,0]) complete();


module complete(uncombined=1, poles=1, roof=1, roofShadow=0)
{
house();
//roof
if (roof) {
translate([200,0,2550]) rotate([0,-22,0]) translate([-1240,-16700,0]) cube([14670/2,19500,300]);
translate([11200,0,2580]) mirror([1,0,0]) rotate([0,-22,0]) translate([-1320,-16700,0]) cube([14670/2,19500,300]);
}
if (roofShadow) {
translate([-1100,-16700,2100]) cube([13750,19500,3010]);
}
//poles
if (poles) {
translate([5670,-4100,-2800]) cube([250,250,7700]);
translate([5670,2300,-2800]) cube([250,250,7700]);
translate([-220,2300,-2800]) cube([250,250,5500]);
translate([10900,2300,-2800]) cube([250,250,5500]);
}
//ground
difference() {
*translate([-330,-17750,-100]) cube([11750,18090,100]);
*translate([3800,-11700,-101]) cube([800,3300,102]);
}
*translate([-330,-17750,-2760]) cube([11750,18090,100]);
translate([120,-120,-2760*uncombined]) basement();
translate([0,0,-1000]) difference() {
translate([-10000,-30750,-100]) cube([30000,40000,100]);
translate([-330,-17750,-100]) cube([11750,18090,100]);
}
}
*translate([250,-250,0]) complete();

*projection(true) translate([250,-250,-1500]) complete(0);

*projection(true) translate([0,0,-200]) rotate([90,0,0])  complete();

module bm() {translate([0,0,-2600]) basement(showDoors=false, 
    showDoorMeasures=false, 
    showWindows=false,
    showWindowMeasures=false,
    showMeasures=false,
    showAreas=false);
}
*bm();
 *house(showDoors=true, 
    showDoorMeasures=true, 
    showWindows=false,
    showWindowMeasures=true,
    showMeasures=true,
    showAreas=true);
translate([0,0,-2600]) basement(showDoors=true, 
    showDoorMeasures=true, 
    showWindows=true,
    showWindowMeasures=true,
    showMeasures=true,
    showAreas=true);
*translate([0,-8100,2500]) cube([4700,3500,200]);