color("red") wall([20,1,4]);

rotate([0,0,90])
rotate([0,0,180])
translate([-20-1,-1,0])
{
wall([20,1,4]);
}

module wall(dim)
{
color("black")  translate([3,0,0.5]) rotate([90,0,0]) text("FRONT", 3);
color("black") translate([4,dim[1],0.5])  rotate([90,0,0]) text("BACK", 3);
cube(dim);
}