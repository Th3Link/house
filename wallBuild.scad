epsilon = 0.001;
*projection(true) union() {
cube([10,1,10]);
translate([0,1+epsilon,0]) cube([10,1-epsilon,10]);
translate([0,2+epsilon,0]) cube([10,1-epsilon,10]);
translate([0,3+epsilon,0]) cube([10,1-epsilon,10]);
translate([0,4+epsilon,0]) cube([10,1-epsilon,10]);
}

kvhD = 1000;
wallL = 3500;
wallH = 2500;
outerIso = 40;
innerIso = 40;
kvh = 200;
kvh2 = 160;
osb = 15;
gips = 10;
board = 20;
fassade = 30;
module wall1(d, x=1)
{
    for (i= [0 : kvhD : 3*kvhD]) {
        translate([0,d*(1-x)-epsilon,0]) rotate([0,0,270*(1-x)]) translate([0,i,0]) cube([d-epsilon,80-epsilon,wallH]);
    }
}

module fassadenWand(s=0){
    color("yellow") cube([board-epsilon,wallL,80]);
color("yellow") translate([0,0,450]) cube([board-epsilon,wallL,80]);
color("yellow") translate([0,0,950]) cube([board-epsilon,wallL,80]);
color("yellow") translate([0,0,1450]) cube([board-epsilon,wallL,80]);
color("yellow") translate([0,0,1950]) cube([board-epsilon,wallL,80]);
color("yellow") translate([0,0,2400]) cube([board-epsilon,wallL,80]);
color("blue") translate([-board,-board,0]) cube([board-epsilon,80,wallH]);
color("blue") translate([-board,500,0]) cube([board-epsilon,80,wallH]);
color("blue") translate([-board,1000,0]) cube([board-epsilon,80,wallH]);
color("blue") translate([-board,1500,0]) cube([board-epsilon,80,wallH]);
color("blue") translate([-board,2000,0]) cube([board-epsilon,80,wallH]);
color("blue") translate([-board,2500,0]) cube([board-epsilon,80,wallH]);
color("blue") translate([-board,3000,0]) cube([board-epsilon,80,wallH]);
color("green") translate([-board-fassade-epsilon,-board-(board+fassade)*s,0]) cube([fassade-epsilon,wallL,wallH]);    
}

module wallBuild() {
translate([-board,-board,0]) fassadenWand();
translate([0,0,0]) rotate([0,0,270]) mirror([1,0,0]) translate([-board,0,0]) fassadenWand(1);
color("red") translate([0,outerIso,0]) cube([outerIso-epsilon,wallL,wallH]);

color("green") translate([outerIso,outerIso,0]) wall1(kvh);
color("orange") translate([outerIso+kvh,outerIso,0]) cube([osb-epsilon,wallL,wallH]);
color("blue") translate([outerIso+kvh+osb,outerIso+osb+kvh,0]) cube([innerIso-epsilon,wallL,wallH]);
color("yellow") translate([outerIso+kvh+osb+innerIso,outerIso+osb+kvh+innerIso,0]) cube([osb-epsilon,wallL,wallH]);
color("red") translate([outerIso+kvh+osb+innerIso+osb,outerIso+osb+kvh+innerIso+osb,0]) cube([gips-epsilon,wallL,wallH]);


color("yellow") cube([wallL,outerIso-epsilon,wallH]);
color("red") translate([kvh+osb+outerIso,outerIso,0]) wall1(kvh,0);
color("purple") translate([kvh+osb+outerIso,outerIso+kvh,0]) cube([wallL,osb-epsilon,wallH]);
color("pink") translate([kvh+osb+outerIso+innerIso,outerIso+kvh+osb,0]) cube([wallL,innerIso-epsilon,wallH]);
color("black") translate([kvh+osb+outerIso+innerIso+osb,outerIso+kvh+osb+innerIso,0]) cube([wallL,osb-epsilon,wallH]);
color("green") translate([kvh+osb+outerIso+innerIso+osb+gips,outerIso+kvh+osb+innerIso+osb,0]) cube([wallL,gips-epsilon,wallH]);

translate([outerIso+kvh+osb+innerIso,2500,0]) wall1(kvh2,0);
color("green") translate([kvh+osb+outerIso+innerIso,2500-osb,0]) cube([wallL,osb-epsilon,wallH]);
color("black") translate([kvh+osb+outerIso+innerIso+osb,2500-osb-gips,0]) cube([wallL,gips-epsilon,wallH]);

color("green") translate([kvh+osb+outerIso+innerIso,2500+kvh2,0]) cube([wallL,osb-epsilon,wallH]);
color("black") translate([kvh+osb+outerIso+innerIso+osb,2500+kvh2+osb,0]) cube([wallL,gips-epsilon,wallH]);
}
projection(true) wallBuild();