use <isocpeur.ttf>

epsilon = 0.01;
dd = 60; // drawing depth
rahmen = 80;
brh = 900;
free  = 15;
b = [1400];
h = 1400.1;
d = 200;

module window(brh, widthList, height, depth, flipy=false, flipText=false, withText=true, visible=true, id="A") {
if (visible) {
translate([0,(depth-dd)/2,brh+free*(brh ? 1 : 0)]) union() {
cube([rahmen-epsilon,dd,height]);
translate([rahmen,0,0]) cube([widthList[0]-2*rahmen-epsilon,dd/2-epsilon,height]);
translate([rahmen,dd/2,0]) cube([widthList[0]-2*rahmen-epsilon,dd/2-epsilon,height]);
translate([widthList[0]-rahmen,0,0]) cube([rahmen-epsilon,dd,height]);
if (withText) {
f = (flipText ? (flipy ? -1 : 1) : (flipy ? 1 : -1));
f2 = (flipText == flipy ? -1 : 1);
upperText = str(id, " ", str(widthList[0]/10));
translate([widthList[0]/2,f2*(depth/2+f2*f2*120),0]) rotate([0,0,270-(flipy ? 180 : 0)]) windowText(upperText,str(height/10), str(brh/10), height, flipText);
}
}
}
}

module windowText(topText, bottomText, brh, height, flip=false, lineWidth=1000) {
    textSize = 230;
    translate([(flip ? -lineWidth : 0),0,0]) linear_extrude(height) {
    translate([(flip ? lineWidth : 0), -textSize/5,0])
        rotate([0,0,90]) offset(delta=-textSize/27) text("BRH", size=textSize, halign="right",
         valign=(flip ? "bottom" : "top"), font="isocpeur");
         
    translate([lineWidth*(flip ? 1/3 : 2/3),textSize/5,0]) offset(delta=-textSize/27) text(topText, size=textSize, halign="center", valign="bottom", font="isocpeur");
    offset(delta=-textSize/27) projection(true) rotate([0,90,0]) cylinder(r=textSize/20,h=lineWidth);
   
        translate([(flip ? lineWidth : 0), textSize/5,0])
        rotate([0,0, 90]) offset(delta=-textSize/27) text(brh, size=textSize, halign="left",
         valign=(flip ? "bottom" : "top"), font="isocpeur");
        
    translate([lineWidth*(flip ? 1/3 : 2/3),textSize/-5,0]) offset(delta=-textSize/27) text(bottomText, size=textSize, halign="center", valign="top", font="isocpeur");
    }
}

*projection(true) translate([0,0,-1000]) window(brh=brh, widthList=b, height=h, depth=d, flip=true);
window(brh=brh, widthList=b, height=h, depth=d, flipy=false, flipText=true);
*translate([2000,0,0]) window(brh=0, widthList=[900], height=2100, depth=d);

*windowText("uiea","uaie", "0", 400, true);
*translate([0,0,0]) windowText("uiea","uaie", "0", 400, false);
